<?php

namespace avalcelular;

use avalcelular\Session,
    avalcelular\Common,
    avalcelular\ReCaptcha;

class Controller
{

    protected $session;
    protected $model;

    public function __construct()
    {
        Session::inicializar();
        self::loadModel('Application\Model\ModelConfig', 'modelsistema');
    }

    protected function loadView($nome, $vars = null)
    {
        if (is_array($vars) && count($vars) > 0) {
            extract($vars, EXTR_PREFIX_SAME, 'data');
        }

        $arquivo = VIEW_PATH . '/' . $nome . '.phtml';

        if (!file_exists($arquivo)) {
            $this->error("Houve um erro. Essa View {$nome} nao existe.");
        }

        require_once($arquivo);
    }

    protected function loadModel($nome, $apelido = "")
    {
        $this->$nome = new $nome();

        if ($apelido !== '') {
            $this->$apelido = &$this->$nome; //Passando por referencia
        }
    }

    protected function error($msg)
    {
        throw new \Exception($msg);
    }

    protected function validarRetornoServico($retorno = array())
    {

        if ($retorno['O_WRET_COD'] > ZERO) {
            Common::gerarMensagem(OPCAOAVISO, $retorno['O_ARET_DESC_CURTO']);
        }
    }

    protected function carregarMenu()
    {
        $request = new Request();
        $token = Session::get('token');
        $dados['logado'] = true;

        $dados['controller'] = $request->getControlador();
        $dados['metodo'] = $request->getMetodo() == 'main' ? $request->getControlador() : $request->getMetodo();
        $dados['menu'] = !empty($token) ? Common::retornoWSLista($this->modelsistema->MenuPrincipal($token)) : [];
        $dados['config'] = Common::retornoWSLista($this->modelsistema->Configuracoes($token));
        $dados['contatosite'] = Common::retornoWSLista($this->modelsistema->ContatosSite());
        $dados['selecionado'] = !empty(Session::get("selecionado")) ? Session::get("selecionado") : 0;
        $dados['usuariologado'] = !empty($token) ? Common::retornoWSLista($this->modelsistema->UsuarioLogado($token)) : [];

        return $dados;
    }

    protected function prepararView($view, $dados = array())
    {

        if (empty($dados['menu'])) {
            $dados += $this->carregarMenu();
        }

        $this->loadView("Template/cabecalho", $dados);
        $this->loadView("Template/mainmenu", $dados);
        $this->loadView("Template/topbar", $dados);
        $this->loadView($view, $dados);
        $this->loadView("Template/footerbar", $dados);
        $this->loadView("Template/rodape", $dados);
    }

    protected function prepararViewLogin($view, $dados = array())
    {
        $request = new Request();
        $dados['controller'] = $request->getControlador();
        $dados['metodo'] = $request->getMetodo() == 'main' ? $request->getControlador() : $request->getMetodo();
        $dados['config'] = Common::retornoWSLista($this->model->Configuracoes());
        $dados['contatosite'] = Common::retornoWSLista($this->modelsistema->ContatosSite());

        $this->loadView("Template/cablogin", $dados);
        $this->loadView($view, $dados);
        $this->loadView("Template/rodapelogin", $dados);
    }

    protected function prepararViewSite($view, $dados = array())
    {
        //$request = new Request();
        //$dados['controller'] = $request->getControlador();
        //$dados['metodo'] = $request->getMetodo() == 'main' ? $request->getControlador() : $request->getMetodo();

        if (empty(Session::get('logotipo'))) {
            $config = Common::retornoWSLista($this->model->Configuracoes());
            Session::set('logotipo',$config['LOGOTIPO']);
        }

        if (empty(Session::get('favicon'))) {
            $config = Common::retornoWSLista($this->model->Configuracoes());
            Session::set('favicon',$config['FAVICON']);
        }

        $dados['config']['LOGOTIPO'] = Session::get('logotipo');
        $dados['config']['FAVICON'] = Session::get('favicon');
        $this->loadView("Template/cabecalhosite", $dados);
        $this->loadView("Template/topbarsite", $dados);
        $this->loadView($view, $dados);
        $this->loadView("Template/footerbarsite", $dados);
        $this->loadView("Template/rodapesite", $dados);
    }

    protected function prepararBasicView($view, $dados = array())
    {
        $this->loadView($view, $dados);
    }

    protected function loadException(\Exception $msg, $voltar = TRUE)
    {
        Common::gerarMensagem(OPCAOERRO, $msg->getMessage(), $voltar);
    }

    function valida_cep_correio($cep)
    {
        $url = 'https://viacep.com.br/ws/' . $cep . '/json/';

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);

        if ($result !== FALSE) {
            $ret = json_decode($result, true);
        } else {
            $ret = curl_error($ch);
        }

        return $ret;
    }

    function sair()
    {
        Session::destroy();
        Common::retornarPagina(get_called_class());
    }

    function salvarImagem($file, $pasta, $config)
    {
        $Imagem = new \Application\Controller\Common\Imagem($config);

        if (!file_exists(IMAGENS_PATH . '/' . $pasta)) {
            mkdir(IMAGENS_PATH . '/' . $pasta, 0777, true);
        }

        return $Imagem->executar(IMAGENS_PATH . '/' . $pasta, $file);
    }

    function removerImagem($nome_imagem)
    {
        $caminho_imagem = IMAGENS_PATH . "/" . $nome_imagem;
        if (file_exists($caminho_imagem))
            @unlink($caminho_imagem);
        clearstatcache(TRUE, $caminho_imagem);
    }
}
