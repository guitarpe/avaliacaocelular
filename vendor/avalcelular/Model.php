<?php

namespace avalcelular;

use avalcelular\Database,
avalcelular\Common;

require_once ('Base.php');

class Model
{
    function __construct()
    {
        try {
            $conn = $this->db = new Database(DB_HOST, DB_PORT, DB_NAME, DB_USER, DB_PASS);
            $conn->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

        } catch (\PDOException $e) {
            return $e->getMessage();
        } finally {
            $conn = null;
        }
    }

    protected function selectData($sql, $parametros = [])
    {
        try {
            $erro = false;
            $message = 'Falha na consulta ';
            $data = [];

            $result = $this->db->select($sql, $parametros);

            if (!empty($result)) {
                $data = $result;
                return ["erro" => $erro, "message" => '', "list" => $data];
            } else {
                return ["erro" => $erro, "message" => 'Sem resultados', "list" => $data];
            }

            return ["erro" => true, "message" => $message, "list" => $data];
        } catch (\PDOException $e) {
            echo $e->getMessage();
        }
    }

    public function insertData($table, $parametros = [])
    {
        $erro = false;
        $message = '';
        $data = [];

        $result = $this->db->insert($table, $parametros);

        if ($result != 0) {
            return ["erro" => $erro, "message" => $message, "list" => $data];
        }

        $this->db->rollback();
        $msgErro = $this->db->getErro();
        $message = "Falha no insert " . $msgErro['message'];

        return ["erro" => true, "message" => $message, "list" => $data];
    }

    public function updateData($table, $parametros, $where)
    {
        $message = '';
        $data = [];
        $erro = false;

        $result = $this->db->update($table, $parametros, $where);

        if ($result != 0) {
            return ["erro" => $erro, "message" => $message, "list" => $data];
        }

        $this->db->rollback();
        $msgErro = $this->db->getErro();
        $message = "Falha no update " . $msgErro['message'];

        return ["erro" => true, "message" => $message, "list" => $data];
    }

    public function deleteData($table, $parametros = [])
    {
        $erro = false;
        $message = '';
        $data = [];

        $result = $this->db->delete($table, $parametros);

        if ($result != 0) {
            return ["erro" => $erro, "message" => $message, "list" => $data];
        }

        $this->db->rollback();
        $msgErro = $this->db->getErro();
        $message = "Falha no update " . $msgErro['message'];

        return ["erro" => true, "message" => $message, "list" => $data];
    }

    public function callprocedure($sql, $parametros)
    {
        $erro = false;
        $message = '';
        $data = [];

        $result = $this->db->executeProcedure($sql, $parametros);

        if ($result != 0) {
            $data = $result;
            return ["erro" => $erro, "message" => $message, "list" => $data];
        } else {
            $message = "Falha ao executar procedure";
        }

        return ["erro" => true, "message" => $message, "list" => $data];
    }

    public function Configuracoes($token = null)
    {
        if(empty($token)){
            $parametros = [];

            $sql = "SELECT
                        URL_SISTEMA,
                        LOGOTIPO,
                        FAVICON,
                        0 as O_COD_RETORNO,
                        '' as O_DESC_CURTO,
                        'N' as O_TOKEN_INVALIDO
                    FROM CONFIGURACOES LIMIT 1";

            $resultado = $this->selectData($sql, $parametros);
        }else{

            $parametros = [
                'I_TOKEN' => $token
            ];

            $sql = "CALL PRC_DADOS_CONFIG(:I_TOKEN)";
            $resultado = $this->callprocedure($sql, $parametros);
        }

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function MenuPrincipal($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_EXIBIR_MENU(:I_TOKEN)";
        $resultado = $this->callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ContatosSite()
    {
        $parametros = [];

        $sql = "SELECT
                    EMAIL_TECNICO, ENDERECO, BAIRRO,
                    NUMERO, CIDADE, UF, CEP, COMPLEMENTO,
                    TELEFONE, 0 as O_COD_RETORNO,
                    '' as O_DESC_CURTO, 'N' as O_TOKEN_INVALIDO
                FROM CONTATO;";

        $resultado = $this->selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function UsuarioLogado($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_DADOS_USUARIO_LOGADO(:I_TOKEN)";
        $resultado = $this->callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaMenusCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_MENUS(:I_TOKEN);";
        $resultado = $this->callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    protected function verificarToken($list)
    {
        $token = null;

        if ($list['erro'] == 1) {
            $token = $list['att_token'];
        } else {

            foreach ($list['list'] as $key => $data) {
                if (is_array($data)) {
                    foreach ($data as $key => $dados) {
                        $token = $data['O_TOKEN_INVALIDO'];
                        break;
                    }
                } else {
                    $token = $list['list']['O_TOKEN_INVALIDO'];
                    break;
                }
            }
        }

        return $token;
    }
}
