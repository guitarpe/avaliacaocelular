<?php
/* --------------------------SUCESSO--------------------------------- */

const CAPTCHA_SECRET = "6LcUsFIUAAAAAAzDafDMPfA0bKOh29EB4sQf40Zh";
const MGS_SUCESSO_REGISTRO_ATUALIZADO = 'Registro atualizado com sucesso!';
const MGS_SUCESSO_REGISTRO_CADASTRADO = 'Registro cadastrado com sucesso!';
const MGS_SUCESSO_REGISTRO_DELETADO = 'Registro deletado com sucesso!';
const MGS_SUCESSO_REGISTRO_CANCELADO = 'Registro cancelado com sucesso!';
const MGS_SUCESSO_REGISTRO_ATIVADO = 'Registro Ativado com Sucesso!';
const MGS_SUCESSO_REGISTRO_INATIVADO = 'Registro Inativado com Sucesso!';
const MGS_SUCESSO_REGISTRO_HABILITADO = 'Registro Habilitado com Sucesso!';
const MGS_SUCESSO_REGISTRO_DESABILITADO = 'Registro Desabilitado com Sucesso!';

/* --------------------------ERROR------------------------------------- */
const MSG_ERROR_FOI_POSSIVEL_VER_DETALHES = 'Não foi possível visualizar os detalhes deste registro!';
const MSG_ERROR_FOI_POSSIVEL_VER_DADOS = 'Não foi possível visualizar os dados deste registro!';
const MSG_ERROR_CADASTROU_REGISTRO = 'Não foi possível cadastrar este registro!';
const MSG_ERROR_FOI_POSSIVEL_ATUALIZAR = 'Não foi possível atualizar este Registro!';
const MSG_ERROR_FOI_POSSIVEL_DELETAR = 'Não foi possível deletar este Registro!';
const MSG_ERROR_FOI_POSSIVEL_CANCELAR = 'Não foi possível cancelar este Registro!';
const MSG_ERROR_ATIVAR = 'Não foi possível Ativar o Registro!';
const MSG_ERROR_INATIVAR = 'Não foi possível inativar o Registro!';
const MSG_ERROR_DOWNLOAD = 'Não foi possível realizar o Download !';
const MSG_ERROR_HABILITAR = 'Não foi possível Habilitar o Registro!';
const MSG_ERROR_DESABILITAR = 'Não foi possível Desabilitar o Registro!';
const MSG_ERROR_EXCECAOPADRAO = "Aviso: Falha(s) no Sistema, foi enviado um email sobre o problema ao departamento de TI.";

/* ------------------------------ALERT--------------------------------- */
const MSG_ALERT_CNPJ_INVALIDO = 'CNPJ Inválido!';
const MSG_ALERT_JA_EXISTE_CNPJ_BASE = 'Já existe CNPJ cadastrado na base!';
const MSG_ALERT_NAO_EXISTE_CPNPJ_BASE = 'Não existe CNPJ cadastrado na base, não pode continuar com a operação!';
const MSG_ALERT_CAMPO_OBRIGATORIO = 'Campo obrigatório não preenchido!';
const MSG_ALERT_CAMPOS_OBRIGATORIOS = "Os campos a seguir são obrigatórios:";
const MSG_ALERT_LOJISTA_INEXISTENTE = "Logísta inexistente!:";
const MSG_ERRO_EXCECAOPADRAO = "Aviso: Falha(s) no Sistema, foi enviado um email sobre o problema ao departamento de TI.";
const AVISO_MSG_CAPTCHAOBRIGATORIO = "É necessário validar o captcha!";

/* ------------------------------EXTRAS--------------------------------- */
const MSG_SEM_FILTROS = 'Sem aplicar filtro, mostrará apenas os últimos 20 Registros!';
const MSG_CLIENTE_INEXISTENTE = 'Cliente não encontrado!';
const OPCAOSUCESSO = 1;
const OPCAOAVISO = 2;
const OPCAOERRO = 3;
const ZERO = 0;
const PAGINACAO = 10;

define("CONST_PROXIMAATUALIZACAOMINUTOS", 5);
define("CONST_AGRUPAMENTOCONSULTA5SEG", 12); /* Agrupamento das consultas: 12 -> de 5 em 5 segundos; 6 -> de 10 em 10 segundos; 2 -> de 30 em 30 segundos; 1 -> de 1 em 1 minuto */
define("CONST_AGRUPAMENTOCONSULTA10SEG", 6); /* Agrupamento das consultas: 12 -> de 5 em 5 segundos; 6 -> de 10 em 10 segundos; 2 -> de 30 em 30 segundos 1 -> de 1 em 1 minuto */
define("CONST_AGRUPAMENTOCONSULTA30SEG", 2); /* Agrupamento das consultas: 12 -> de 5 em 5 segundos; 6 -> de 10 em 10 segundos; 2 -> de 30 em 30 segundos 1 -> de 1 em 1 minuto */
define("CONST_AGRUPAMENTOCONSULTA1MIN", 1); /* Agrupamento das consultas: 12 -> de 5 em 5 segundos; 6 -> de 10 em 10 segundos; 2 -> de 30 em 30 segundos 1 -> de 1 em 1 minuto */

/* INTEGRAÇÃO COM SISTEMA CETRUS */
define('INT_ID_USUARIO', 15);
define('INT_ID_SENHA', 'site321');
define('INT_TABELA', 'PRODUTOS');
define('URL_API_CONSULTAR', '/datasnap/rest/TSM/LojaVirtualConsultarProdutos_V01/'); //{"IdUsuario": "15", "Senha": "site321", "Produto_Id": ""}');
define('URL_APU_CONSUMIR', '/datasnap/rest/TSM/LojaVirtual_ConsumirWeb_V01/'); //{"IdUsuario": "15", "Senha": "site321", "Tabela": "PRODUTOS", "Chave_Id": "100-115"}');

//{"error":"TSM.updateLojaVirtual_ConsumirWeb_V01 method not found in the server method list"}
