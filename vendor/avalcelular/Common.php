<?php

namespace avalcelular;

use DateTime,
    avalcelular\PHPMailer;
use Exception;

class Common extends \Exception
{

    static function exibirDadosTeste($dados){
        echo '<pre>';
        print_r($dados);
        echo '</pre>';
        exit();
    }

    static function returnValor($str)
    {
        return str_replace(",", ".", str_replace(".", "", $str));
    }

    static function encrypt_decrypt($action, $string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'foottsdevsis';
        $secret_iv = 'foottsdevsis';

        $key = hash('sha256', $secret_key);

        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    static function montarDadosParaEmail($dados = [], $titulo = "", $info = [], $tipo = null)
    {
        $assunto = ($titulo == "") ? "Falha no Sistema!" : $titulo;

        $ip = self::getUserIP();
        $email = $dados['EMAIL'];

        if (isset($dados['US_ID'])) {
            $id = $dados['US_ID'];
        }

        if ($tipo == 2 || $tipo == 3) {
            $link = URL . SITE_URL . "/Login/Reset/" . self::encrypt_decrypt('encrypt', $id);
        }

        $html = '<html>
                    <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
                        <title>Avaliação de avalcelular - Notificações automática</title>
                    </head>
                    <body>
                        <div class="container" style="width:600px; margin-left:auto; margin-right:auto; background-color: #f2f2f2">
                            <div class="container" style="background-color: #ffffff; border: #ccc solid 1px">
                                <img src="' . URL_IMG_MAIL . '/' . $info['logotipo'] . '">
                            </div>
                            <div class="container" style="padding:20px">
                                <table>
                                    <tr>
                                        <td>
                                            <h2><b>Avaliação de avalcelular - Notificações automática</b></h2>
                                            <br/>';
        if ($tipo == 2) {
            $html .= 'Olá, Você realizou um reset de senha.<br/>';
            $html .= 'Para gerar uma nova senha você deve clicar no link abaixo<br/><br/>';
            $html .= 'E-mail: ' . $email . '<br/>';
            $html .= 'IP: ' . $ip . ' <br/><br/>';
            $html .= 'Gere uma nova senha clicando aqui <a href="' . $link . '" target="_blank">Gerar Senha</a><br/>';
        } else if ($tipo == 4) {
            $html .= 'Olá, Você realizou um cadastro em nosso site.<br/>';
            $html .= 'A partir de agora você terá acesso ao sistema<br/><br/>';
            $html .= 'A equipe lhe deseja boas vindas<br/><br/>';
        }

        $html .= '</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </body>
                </html>';

        return self::configurarEmail($assunto, $html, $info);
    }

    static function getUserIP()
    {
        $client = @$_SERVER['HTTP_CLIENT_IP'];
        $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
        $remote = $_SERVER['REMOTE_ADDR'];

        if (filter_var($client, FILTER_VALIDATE_IP)) {
            $ip = $client;
        } elseif (filter_var($forward, FILTER_VALIDATE_IP)) {
            $ip = $forward;
        } else {
            $ip = $remote;
        }

        return $ip;
    }

    private static function configurarEmail($assunto, $html, $info = [])
    {
        $mail = new PHPMailer();

        $mail->IsSMTP();
        $mail->SMTPAuth = true;
        //$mail->SMTPDebug = 2;
        $mail->SMTPSecure = "ssl";
        $mail->Host = $info['host'];
        $mail->Port = $info['porta'];
        $mail->SMTPAuth = true;
        $mail->Username = $info['username'];
        $mail->Password = $info['senha'];
        $mail->From = $info['from'];
        $mail->FromName = $info['fromname'];
        $mail->IsHTML(true);
        $mail->CharSet = 'utf-8';
        $mail->Subject = $assunto;
        $mail->Body = $html;

        return $mail;
    }

    static function dispararEmail($dados = [], $email = "", $titulo = "", $info = [], $tipo = null)
    {
        if (ENVIA_EMAIL) {
            $mail = self::montarDadosParaEmail($dados, $titulo, $info, $tipo);
            $mail->AddAddress($email, $dados['NOME_COMPLETO']);
            $envio = $mail->Send();

            $mail->ClearAllRecipients();
            $mail->ClearAttachments();

            return $envio;
        } else {
            return true;
        }
    }

    static function dispararEmailPersonalizado($assunto, $html, $email, $info)
    {
        if (ENVIA_EMAIL) {
            $mail = self::configurarEmail($assunto, $html, $info);
            $mail->AddAddress($email, $email);
            $envio = $mail->Send();
            $mail->ClearAddresses();

            return $envio;
        } else {
            return true;
        }
    }

    static function validarEmail($email)
    {
        if (!preg_match("/^[a-zA-Z0-9][a-zA-Z0-9\._-]+@([a-zA-Z0-9\._-]+\.)[a-zA-Z-0-9]{2,3}$/", $email)) {
            self::gerarMensagem(OPCAOAVISO, 'Email inválido');
        }
    }

    static function tratarCampoTipoArray($dados = [])
    {
        $resultado = '';
        foreach ($dados as $dado) {
            $resultado .= $dado . ',';
        }

        $resultado = rtrim($resultado, ',');

        return $resultado;
    }

    static function camuflarSenha($string)
    {
        $string = preg_replace('/[^0-9]/', '', hash('sha512', $string));
        $string = strtoupper(preg_replace('/[^a-z]/', '', hash('sha512', $string)));
        $string = base64_encode($string);
        $string = substr($string, 0, 6);

        return $string;
    }

    static function gerarSenha($tamanho = 8, $maiusculas = true, $numeros = true, $simbolos = false)
    {
        $lmin = 'abcdefghijklmnopqrstuvwxyz';
        $lmai = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $num = '1234567890';
        $simb = '!@#$%*-';
        $retorno = '';
        $caracteres = '';

        $caracteres .= $lmin;

        if ($maiusculas)
            $caracteres .= $lmai;
        if ($numeros)
            $caracteres .= $num;
        if ($simbolos)
            $caracteres .= $simb;

        $len = strlen($caracteres);

        for ($n = 1; $n <= $tamanho; $n++) {
            $rand = mt_rand(1, $len);
            $retorno .= $caracteres[$rand - 1];
        }

        return $retorno;
    }

    public static function calcValodFrete($tp_serv, $cep_o, $cep_d, $peso, $altura, $largura, $comprimento, $diametro)
    {
        $retorno = [];

        $valores = [
            'nCdEmpresa'            => "",
            'sDsSenha'              => "",
            'nCdServico'            => $tp_serv,
            'sCepOrigem'            => $cep_o,
            'sCepDestino'           => $cep_d,
            'nVlPeso'               => $peso,
            'nCdFormato'            => 1,
            'nVlComprimento'        => $comprimento,
            'nVlAltura'             => $altura,
            'nVlLargura'            => $largura,
            'nVlDiametro'           => 0,
            'sCdMaoPropria'         => 'N',
            'nVlValorDeclarado'     => 0,
            'sCdAvisoRecebimento'   => 'N',
            'StrRetorno'            => 'xml',
            'nIndicaCalculo'        => 3,
            'nVlDiametro'           => $diametro
        ];

        $webserviceUrl = 'http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx';
        $queryString = http_build_query($valores);

        $url = "{$webserviceUrl}?{$queryString}";

        $cURL = curl_init();
        curl_setopt_array($cURL, array(
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_URL => $url
        ));
        $respostaWebservice = curl_exec($cURL);
        curl_close($cURL);

        if ($respostaWebservice) {

            $resposta = simplexml_load_string($respostaWebservice);

            if ($resposta) {


                $dadosFrete = $resposta->cServico;

                switch ((string) $dadosFrete->Erro) {
                    case 0:
                        $retorno['ERRO'] = 0;
                        $retorno['MSG'] = $dadosFrete->obsFim;
                        $retorno['PRAZO'] = json_decode($dadosFrete->PrazoEntrega, 0);
                        $retorno['VALOR'] = self::formataValorEmReais((string) $dadosFrete->Valor);

                        break;
                    case 10:
                        $retorno['ERRO'] = 0;
                        $retorno['MSG'] = $dadosFrete->obsFim;
                        $retorno['PRAZO'] = json_decode($dadosFrete->PrazoEntrega, 0);
                        $retorno['VALOR'] = self::formataValorEmReais((string) $dadosFrete->Valor);

                        break;
                    case 7:
                        $retorno['ERRO'] = $dadosFrete->Erro;
                        $retorno['MSG'] = 'Serviço temporariamente indisponível.';
                        $retorno['PRAZO'] = 0;
                        $retorno['VALOR'] = 0;

                    default:
                        $retorno['ERRO'] = $dadosFrete->Erro;
                        $retorno['MSG'] = 'Não foi possível obter o valor do frete pelos Correios.';
                        $retorno['PRAZO'] = 0;
                        $retorno['VALOR'] = 0;
                }
            }
        }

        return $retorno;
    }

    static function retornarPagina($url = "")
    {
        header('Location: ' . SITE_URL . '/' . $url);
        exit;
    }

    static function voltar()
    {
        if (empty($_SERVER['HTTP_REFERER'])) {
            $_SERVER['HTTP_REFERER'] = SITE_URL;
        }

        header('Location: ' . $_SERVER['HTTP_REFERER']);
        exit;
    }

    static function validarData($date = null)
    {
        $d = DateTime::createFromFormat('d/m/Y', $date);
        return ($d && $d->format('d/m/Y') === $date);
    }

    static function validarCampoObrigatorio($dados = [], $apelido = FALSE)
    {
        $msg = 0;
        $campo = "<ul>";
        $i = 0;
        if (!empty($dados)) {
            foreach ($dados as $key => $value) {
                $i++;
                if (empty($value)) {
                    $msg += 1;
                    if ($apelido) {
                        $key = $dados['#APELIDO#'][$i - 1];
                    }
                    $campo .= "<li>" . ucfirst($key) . "</li>";
                }
            }
            $campo = MSG_ALERT_CAMPOS_OBRIGATORIOS . $campo;
        } else {
            $msg = 1;
            $campo = "<ul> ACESSO NÃO PERMITIDO!";
        }

        if ($msg > 0) {
            self::gerarMensagem(OPCAOAVISO, $campo . "</ul>");
        }

        unset($dados['#APELIDO#']);
        return $dados;
    }

    static function StrReplaceMultiplo($parametros, $subject)
    {
        foreach ($parametros as $key => $value) {
            $subject = str_replace($key, $value, $subject);
        }

        return $subject;
    }

    static function contarArray($dados = [])
    {
        $total = (sizeof($dados));
        $n = ZERO;
        foreach ($dados as $value) {
            if (empty($value)) {
                $n++;
            }
        }

        return $total - $n;
    }

    static function removerCaracteresEspeciais($nome)
    {
        $nome = strtolower($nome);
        $string = preg_replace("/[^a-zA-Z0-9_]/", "", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN-"));
        return str_replace('-', ' ', $string);
    }

    static function removerEspacosPontos($nome)
    {
        $string = preg_replace("/[^a-zA-Z0-9]/", "-", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN-"));
        return str_replace('_', '-', $string);
    }

    static function somenteNumeros($valor)
    {
        $string = preg_replace("/[^0-9]/", "", $valor);
        return $string;
    }

    static function retornoNomeDeUsuario($nome)
    {
        $string = preg_replace("/[^a-zA-Z0-9]/", ".", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN."));
        $string = strtolower($string);
        return str_replace('_', '.', $string);
    }

    static function retornoLoginGerado($nome)
    {
        $string = preg_replace("/[^a-zA-Z0-9]/", ".", strtr(utf8_decode($nome), utf8_decode("áàãâéêíóôõúüçñÁÀÃÂÉÊÍÓÔÕÚÜÇÑ "), "aaaaeeiooouucnAAAAEEIOOOUUCN."));
        $string = strtolower($string);
        return substr(str_replace('_', '.', $string), 0, 12);
    }

    static function checkRemoteFile($url)
    {
        $file_headers = @get_headers($url);
        if ($file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return false;
        } else {
            return true;
        }
    }

    static function valida_imagem($caminho, $imagem)
    {
        if (file_exists($caminho . $imagem)) {
            return $caminho . $imagem;
        } else {
            return $caminho . $imagem; //IMGS_URL . '/semimagem.jpg';
        }
    }

    static function formatarData($data)
    {
        return implode('-', array_reverse(explode('/', $data)));
    }

    static function mask($val, $mask)
    {
        $maskared = '';
        $k = 0;

        for ($i = 0; $i <= strlen($mask) - 1; $i++) {
            if ($mask[$i] == '#') {
                if (isset($val[$k]))
                    $maskared .= $val[$k++];
            } else {
                if (isset($mask[$i]))
                    $maskared .= $mask[$i];
            }
        }
        return $maskared;
    }

    public static function validarInputsObrigatorio($dados = [], $redir = null, $modulo = null)
    {
        $qtde = 0;
        $campos = "<ul align='left'>";
        $msg = "Campos obrigatórios não preenchido!";
        $situacao = "danger";
        $acao = 'acao';

        foreach ($dados as $key => $value) {
            if (empty($value)) {

                $qtde += 1;
                $campos .= "<li>" . ucfirst($key) . "</li>";
            }
        }

        $msg .= '<br>' . $campos . "</ul>";

        if ($qtde > 0) {
            self::alert($msg, $situacao, $acao, $modulo);
            self::redir($redir);
        }
    }

    public static function validarAcessoObrigatorio($dados = [], $redir = null)
    {
        $qtde = 0;
        $campos = "<ul align='left'>";
        $msg = "Campos obrigatório não preenchido!";
        $situacao = "alert-danger";
        $acao = 'acao';

        foreach ($dados as $key => $value) {
            if (empty($value)) {

                $qtde += 1;
                $campos .= "<li>" . ucfirst($key) . "</li>";
            }
        }

        $msg .= '<br>' . $campos . "</ul>";

        if ($qtde > 0) {
            self::alert($msg, $situacao, $acao);
            if (empty($redir)) {
                self::voltar();
            } else {
                self::redir($redir);
            }
        }
    }

    public static function validarEmBranco($dado)
    {

        $qtde = 0;

        if (strlen($dado) == 0) {
            $qtde = 1;
        }

        if (($qtde == 1)) {
            $msg = "Campo obrigatório não preenchido!";
            $situacao = "alert-danger";
            $acao = 'acao';
            self::alert($msg, $situacao, $acao);
            self::voltar();
        }
    }

    public static function alert($msg = 'Operação realizada com Sucesso', $situacao = '', $acao = 'acao', $modulo = null)
    {

        if ($situacao == '') {
            $situacao = "success"; //" uk-notify uk-notify-top-center ";
        }
        Session::set($acao, TRUE);
        Session::set("acao-class", $situacao);
        Session::set("acao-msg", $msg);

        if ($modulo != null) {
            Session::set("acao-tab", $modulo);
        }
    }

    static function gerarMensagem($tipoDaMsg, $msg = "", $redirecionar = TRUE)
    {
        Session::set("sessao", TRUE);

        if ($tipoDaMsg === OPCAOSUCESSO) {
            Session::set("class", "success");
            Session::set("msg", '<i class="uk-icon-check"></i> ' . $msg);
        }

        if ($tipoDaMsg === OPCAOAVISO) {
            Session::set("class", "warning");
            Session::set("msg", '<i class="uk-icon-warning"></i> ' . $msg);
        }

        if ($tipoDaMsg === OPCAOERRO) {
            $msg = MSG_ERRO_EXCECAOPADRAO . "<br><br> Causa: " . $msg;
            Session::set("class", "danger");
            Session::set("msg", '<i class="uk-icon-danger"></i> ' . $msg);
        }

        if ($redirecionar) {
            self::voltar();
        }
    }

    public static function redir($url = "", $tipo = null)
    {
        if ($tipo == 1) {
            header('location: ' . $url);
        } else {
            header('location: ' . SITE_URL . '/' . $url);
        }
        exit();
    }

    public static function redirExt($url = "")
    {
        header('location: ' . $url);
        exit();
    }

    public static function retornoWSLista($param, $op = 0)
    {
        if($op == 1){
            return $param['list'];
        }else{
            return !empty($param['list']) ? count($param['list']) > 1 ? $param['list'] : $param['list'][0] : null;
        }
    }

    public static function cartaoCredMasking($number, $maskingCharacter = 'X')
    {
        return !empty($number) ? substr($number, 0, 4) . str_repeat($maskingCharacter, strlen($number) - 8) . substr($number, -4) : '';
    }

    public static function separa_tel_ddd($str)
    {
        return explode(" ", preg_replace("/[^0-9]-/", "", $str));
    }

    public static function tratar_frases_grandes($string, $tam = 50)
    {
        $subtam = $tam - 3;
        return strlen(trim($string)) > $tam ? substr($string, 0, $subtam) . '...' : $string;
    }

    public static function formataValorEmReais($valor)
    {
        $valor = str_replace('.', '', $valor);
        $valor = str_replace(',', '.', $valor);
        return (float) $valor;
    }

    public static function similar_file_exists($filename) {
        if (file_exists($filename)) {
            return true;
        }
        $dir = dirname($filename);
        $files = glob($dir . '/*');
        $lcaseFilename = strtolower($filename);
        foreach($files as $file) {
            if (strtolower($file) == $lcaseFilename) {
            return true;
            }
        }
        return false;
    }
}
