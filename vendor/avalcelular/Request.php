<?php

namespace avalcelular;

class Request
{

    private $_controlador = "index";
    private $_metodo = "main";
    private $_args = array();

    public function __construct()
    {
        // Algum controlador foi informado na URL? se não foi, mantêm que o controlador é o 'index'.
        if (!filter_has_var(INPUT_GET, 'url'))
            return false;

        // Explode os segmentos da URL e os armazena em um Array
        $segmentos = explode('/', filter_input(INPUT_GET, 'url'));

        // Se o controlador foi realmente definido, retorna o nome dele.
        $this->_controlador = ($c = array_shift($segmentos)) ? $c : 'home';

        // Se um método foi realmente requisitado, retorna o nome dele.
        $this->_metodo = ($m = array_shift($segmentos)) ? $m : 'main';

        // Se argumentos adicionais foram definidos, os retorna em Array.
        $this->_args = (isset($segmentos[0])) ? $segmentos : array();
    }

    public function getControlador()
    {
        return $this->_controlador;
    }

    public function getMetodo()
    {
        return $this->_metodo;
    }

    public function getArgs()
    {
        return $this->_args;
    }
}
