<?php
namespace Application\Controller\Common;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Arquivos extends Controller
{

    private $config = array();

    function __construct($configPadrao = array('tamanho' => 4999999))
    {
        parent::__construct();

        $this->config = $configPadrao;
        parent::loadModel("Application\Model\ModelImagem", "model");

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function executar($caminho, $file)
    {
        try {
            $msg = '';

            if ($caminho === null) {
                $msg = "Não foi possível localizar o diretório.";
            } else {
                $msg = $this->validarDocumento($file);
            }

            if (strlen($msg) > 0) {
                Common::alert($msg, 'warning', 'acao');
                Common::voltar();
            }

            $documentosalvo = $this->criarArquivoDiretorio($caminho, $file);

            if (empty($documentosalvo)) {
                $ret = ['erro' => true, 'message' => null, 'list' => ['documento' => null]];
            } else {
                $ret = ['erro' => false, 'message' => null, 'list' => ['documento' => $documentosalvo]];
            }

            return $ret;
        } catch (Exception $ex) {
            $ex->getMessage();
        }
    }

    private function regraNomenclaturaDocumento($file)
    {
        $ext = pathinfo($file["name"], PATHINFO_EXTENSION);
        $nome_arquivo = \md5(uniqid(\time())) . "." . $ext;

        return $nome_arquivo;
    }

    private function criarArquivoDiretorio($caminho, $file)
    {
        $novoNome = $this->regraNomenclaturaDocumento($file);

        $gravado = move_uploaded_file($file["tmp_name"], $caminho . '/' . $novoNome);

        if (!$gravado) {
            $msg = 'Ocorreu um erro no momento de salvar o documento.';
            Common::alert($msg, 'warning', 'acao');
            Common::voltar();
        }

        return $novoNome;
    }

    private function validarDocumento($file)
    {
        $aviso = "";
        $msgErro = [];

        if (empty($file['name'])) {
            $msgErro[] = "É necessário que selecione um documento.";
            goto gerarMsg;
        }

        if (!preg_match('/^[a-zA-Z0-9]+/', $file["name"])) {
            $msgErro[] = "Não é permitido caracteres especiais no nome do documento.";
        }

        if (!preg_match("/^application\/(xls|xlsx|pdf|doc|docx|pps|ppt|cdr|vnd.ms-excel|vnd.openxmlformats-officedocument.spreadsheetml.sheet|vnd.openxmlformats-officedocument.wordprocessingml.document)$/", $file["type"])) {
            $msgErro[] = "Desculpe, isto não é um documento, não foi possível salvar. " . $file["type"];
        } else {
            if ($file["size"] > $this->config["tamanho"]) {
                $tamanho = explode(".", ($this->config["tamanho"] / 1024));
                $msgErro[] = sprintf("Documento com tamanho muito grande! O arquivo deve ser de no máximo %s KB.", $tamanho[0]);
            }
        }

        gerarMsg:
        if (sizeof($msgErro)) {
            foreach ($msgErro as $msg) {
                $aviso .= $msg . "<br>";
            }

            return $msg;
        }
    }

    function excluir($caminho, $docs)
    {
        $msg = 'Erro na esclusão de imagens:\n\n';
        $erro = 0;
        foreach ($docs as $nome => $cod) {
            if (!$this->deletarDocumentoDiretorio($caminho, $nome)) {
                $msg .= $nome . 'Cód:' . $cod . '\n';
                $erro++;
            }
        }
        if ($erro > 0) {

        }
    }

    function deletarDocumentoDiretorio($caminho, $arquivo)
    {
        if (file_exists($caminho . '/' . $arquivo)) {
            return unlink($caminho . '/' . $arquivo);
        }
    }
}
