<?php

namespace Application\Controller\Site;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Site extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelSite', 'model');
    }

    function main()
    {

    }

    function Vender($tipoval = null, $modelo = null, $capacidade = null, $avarias = null)
    {
        $itenstermos = [];

        $dados['tipo'] = '';
        $dados['modelo'] = '';
        $dados['capacidade'] = '';
        $dados['avarias'] = '';

        $pag = 0;
        $redir = 0;

        if (!empty($tipoval)) {
            $dadosaparelho['tipo'] = $tipoval;
            $itenstermos[] = $tipoval;
            $pag = 1;
            $redir = 0;

            $tipo = Common::retornoWSLista($this->model->TipoPorApelido($dadosaparelho['tipo']))['TP_ID'];
            Session::set('tipo_id', $tipo);
        }

        if (!empty($modelo)) {
            $dadosaparelho['modelo'] = $modelo;
            $itenstermos[] = $modelo;

            if(empty(Session::get('modelo_id'))){
                $mod = Common::retornoWSLista($this->model->ModeloPorApelido($dadosaparelho['modelo']))['MOD_ID'];
                Session::set('modelo_id', $mod);
            }else{
                $mod = Session::get('modelo_id');
            }
            $listacapacidades = Common::retornoWSLista($this->model->ListacapacidadesCadastrados($mod), 1);

            if(count($listacapacidades) > 0){
                $pag = 3;
                $redir = 0;
            }else{
                $dadosaparelho['capacidade'] = '';
                $pag = 5;
                $redir = 0;
            }
        }

        if (!empty($capacidade)) {
            $dadosaparelho['capacidade'] = $capacidade;
            $itenstermos[] = $capacidade;

            if(empty(Session::get('modelo_id'))){
                $mod = Common::retornoWSLista($this->model->ModeloPorApelido($dadosaparelho['modelo']))['MOD_ID'];
                Session::set('modelo_id', $mod);
            }else{
                $mod = Session::get('modelo_id');
            }
            $listaavarias = Common::retornoWSLista($this->model->ListaAvariasCadastrados($mod), 1);

            if(count($listaavarias) > 0){
                $pag = 5;
                $redir = 0;
            }else{
                $dadosaparelho['avarias'] = '';
                $pag = 6;
                $redir = 0;
            }
        }

        $avariaspost = filter_input(INPUT_POST, 'avarias', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $infoadicional = filter_input(INPUT_POST, 'infoadicional', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        if (!empty($avariaspost)) {

            if (!empty($infoadicional)) {

                $infoadicional = array_diff($infoadicional, array(''));

                $infoadd = implode(',', $infoadicional);

                Session::delete("infoadd");
                Session::set("infoadd", $infoadd);
            }

            $avariaspost = array_diff($avariaspost, array(''));

            $avarias = implode(',', $avariaspost);

            $itenstermos[] = 'ava-'.$avarias;
            $redir = 1;
        }else{
            if (!empty($avarias)) {
                $avarias = str_replace('ava-', '', $avarias);

                $dadosaparelho['avarias'] = $avarias;
                $itenstermos[] = 'ava-'.$avarias;
                $pag = 6;
                $redir = 0;
            }
        }

        $terms = implode("/", $itenstermos);

        $dados['urlaction'] = !empty($terms) ? SITE_URL . '/Site/Vender/'.$terms : SITE_URL . '/Site/Vender';

        $dados['pagina'] = $pag;

        $dados['titulo_1'] = 'Venda seu iPhone ou iPad';
        $dados['titulo_2'] = 'sem sair de casa';
        $dados['texto_1'] = 'Selecione as especificações e condições do aparelho e descubra quanto vale o seu usado hoje.';
        $dados['texto_2'] = 'A gente coleta seu aparelho diretamente na sua casa (sujeito a restrição de região)';
        $dados['texto_3'] = 'ou visite um loja iHelpU mais perto de você';

        $dados['footertitulo_1'] = 'Como vender seu iPhone e iPad na iHelpU:';

        switch($pag){
            case 0:
                $dados['titulopagina_1'] = 'Selecione seu dispositivo';
                $dados['titulopagina_2'] = 'SELECIONE SEU DISPOSITIVO';
                $dados['footertitulo_2'] = 'Realize uma avaliação online.';
                $dados['footertitulo_3'] = 'Receba sua proposta sem sair de casa.';
                $dados['footertitulo_4'] = 'Solicite coleta via motoboy ou entregue em uma de nossas 9 lojas.';
                $dados['footertitulo_5'] = 'Receba seu dinheiro.';

                $dados['listatipos'] = Common::retornoWSLista($this->model->ListatiposCadastrados(), 1);
            break;
            case 1:

                $tipo = Session::get('tipo_id');

                $dados['titulopagina_1'] = 'Selecione seu modelo';
                $dados['footertitulo_2'] = 'Realize uma avaliação online.';
                $dados['footertitulo_3'] = 'Receba sua proposta sem sair de casa.';
                $dados['footertitulo_4'] = 'Solicite coleta via motoboy ou entregue em uma de nossas 9 lojas.';
                $dados['footertitulo_5'] = 'Receba seu dinheiro.';
                $dados['arrastar'] = 'escolha-modelo';

                $dados['tiposelecionado'] = Common::retornoWSLista($this->model->TipoPorApelido($dadosaparelho['tipo']))['TP_DESC'];
                $dados['listaaparelhos'] = Common::retornoWSLista($this->model->ListamodelosCadastrados($tipo), 1);
            break;
            case 3:

                $modelo = Session::get('modelo_id');

                $dados['titulopagina_1'] = 'Selecione a capacidade do seu dispositivo';
                $dados['footertitulo_2'] = 'Realize uma avaliação online.';
                $dados['footertitulo_3'] = 'Receba sua proposta sem sair de casa.';
                $dados['footertitulo_4'] = 'Solicite coleta via motoboy ou entregue em uma de nossas 9 lojas.';
                $dados['footertitulo_5'] = 'Receba seu dinheiro.';
                $dados['arrastar'] = 'escolha-capacidade';

                $dados['tiposelecionado'] = Common::retornoWSLista($this->model->TipoPorApelido($dadosaparelho['tipo']))['TP_DESC'];
                $dados['modeloselecionado'] = Common::retornoWSLista($this->model->ModeloPorApelido($dadosaparelho['modelo']))['MOD_DESC'];
                $dados['listacapacidades'] = Common::retornoWSLista($this->model->ListacapacidadesCadastrados($modelo), 1);
            break;
            case 5:

                $modelo = Session::get('modelo_id');

                $dados['titulopagina_1'] = 'Ajude-nos a calcular a oferta do seu aparelho.';
                $dados['titulopagina_2'] = 'O seu aparelho possui as seguintes avarias?';
                $dados['footertitulo_2'] = 'Realize uma avaliação online.';
                $dados['footertitulo_3'] = 'Receba sua proposta sem sair de casa.';
                $dados['footertitulo_4'] = 'Solicite coleta via motoboy ou entregue em uma de nossas 9 lojas.';
                $dados['footertitulo_5'] = 'Receba seu dinheiro.';
                $dados['arrastar'] = 'escolha-avarias';

                $dados['tiposelecionado'] = Common::retornoWSLista($this->model->TipoPorApelido($dadosaparelho['tipo']))['TP_DESC'];
                $dados['modeloselecionado'] = Common::retornoWSLista($this->model->ModeloPorApelido($dadosaparelho['modelo']))['MOD_DESC'];
                $dados['listaavarias'] = Common::retornoWSLista($this->model->ListaAvariasCadastrados($modelo), 1);
            break;
            case 6:

                $dados['titulo_1'] = 'Resumo da proposta';
                $dados['texto_1'] = 'Disponibilizamos retirada por motoboy em todas as cidades que temos loja.';
                $dados['texto_2'] = 'Escolha o Prazo de Pagamento de sua Preferência';
                $dados['footertitulo_1'] = 'Posso confiar no iHelpU? É seguro?';
                $dados['footertitulo_2'] = 'Hoje somos a maior assistência técnica para aparelhos Apple no Rio Grande do Sul.';
                $dados['footertitulo_3'] = 'Possuímos 9 lojas em todo o estado e pelas nossas mãos já passaram mais de 130 mil aparelhos';
                $dados['footertitulo_4'] = 'A maior referência em assistência técnica especializada Apple.';
                $dados['arrastar'] = 'escolha-coleta';

                $tipo = Session::get('tipo_id');
                $modelo = Session::get('modelo_id');
                $capacidade = Common::retornoWSLista($this->model->CapacidadePorApelido($modelo, $dadosaparelho['capacidade']));

                $dados['dadosaparelho'] = Common::retornoWSLista($this->model->DadosAparelho($modelo, $dadosaparelho));
                if(!empty($dadosaparelho['avarias'])){
                    $dados['listaavarias'] = Common::retornoWSLista($this->model->ListaAvarias($modelo, $dadosaparelho['avarias']), 1);
                }

                $dadosaparelho['tipo'] = $tipo;
                $dadosaparelho['modelo'] = $modelo;
                $dadosaparelho['capacidade'] = $capacidade['CAP_ID'];
                $dadosaparelho['capacidadevalad'] = $capacidade['CAP_VAL_AD'];
                $dadosaparelho['infoadicional'] = Session::get("infoadd");
                $dadosaparelho['rede'] = '';
                $dadosaparelho['acessorios'] = '';

                $dados['infoproposta'] = $dadosaparelho;
                $dados['listacoletas'] = Common::retornoWSLista($this->model->ListaColetasCadastrados(), 1);
                $dados['listalojas'] = Common::retornoWSLista($this->model->ListaLojasCadastrados(), 1);

                $dados['urlaction'] = SITE_URL . '/Site/Checkout';
                $dados['titlepagina'] = "Resumo da Proposta";

            break;
        }

        if($redir == 1){
            Common::redir(SITE_URL . '/Site/Vender/'.$terms, 1);
        }else{
            parent::prepararViewSite("Site/pag_venda", $dados);
        }
    }

    function Checkout()
    {
        if (empty(Session::get('venda'))) {
            $token = session_id();
            Session::set('venda', $token);
        } else {
            $token = Session::get('venda');
        }

        $tipo = filter_input(INPUT_POST, 'tipo');
        $modelo = filter_input(INPUT_POST, 'modelo');
        $capacidade = filter_input(INPUT_POST, 'capacidade');
        $avarias = filter_input(INPUT_POST, 'avarias');
        $coleta = filter_input(INPUT_POST, 'coleta');
        $valproposta = filter_input(INPUT_POST, 'valproposta');
        $infoadicional = filter_input(INPUT_POST, 'infoadicional');

        $parametros = [
            'SESSAO' => $token,
            'IP' => Common::getUserIP(),
            'TIPO' => $tipo,
            'MODELO' => $modelo,
            'REDE' => '',
            'CAPACIDADE' => $capacidade,
            'ACESSORIOS' => '',
            'AVARIAS' => $avarias,
            'COLETA' => $coleta,
            'VAL_PROPOSTA' => $valproposta,
            'OBSERVACAO' => $infoadicional
        ];

        $ret = Common::retornoWSLista($this->model->RegistrarProposta($parametros));

        if(intval($ret['O_COD_RETORNO']) == 0){
            $dados['titulo_1'] = 'Proposta recebida com sucesso';
            $dados['titulo_2'] = 'O número da sua proposta é: '.str_pad($ret['O_PROP_ID'] , 5 , '0' , STR_PAD_LEFT);
            $dados['texto_1'] = 'Tenha ele sempre em mãos para agilizar seu atendimento, ';
            $dados['texto_2'] = 'ou entre em contato pelo nosso Whatsapp';
            $dados['texto_3'] = 'Próximos passos';
            $dados['footertitulo_1'] = 'Posso confiar no iHelpU? É seguro?';
            $dados['footertitulo_2'] = 'Hoje somos a maior assistência técnica para aparelhos Apple no Rio Grande do Sul.';
            $dados['footertitulo_3'] = 'Possuímos 9 lojas em todo o estado e pelas nossas mãos já passaram mais de 130 mil aparelhos';
            $dados['footertitulo_4'] = 'A maior referência em assistência técnica especializada Apple.';
            $dados['arrastar'] = 'escolha-finalizar';

            $proposta = Common::retornoWSLista($this->model->DadosProposta($ret['O_PROP_ID']));

            $dados['pagina'] = 10;
            $dados['urlaction'] = SITE_URL . '/Site/Finalizar';
            $dados['proposta'] = $proposta;
            $dados['listalojas'] = Common::retornoWSLista($this->model->ListaLojasCadastrados(), 1);
            $dados['listacidades'] = Common::retornoWSLista($this->model->ListaCidadesAbrangenciaCadastrados($proposta['MEIO_ID']), 1);

            Session::delete("infoadd");
            Session::delete('modelo_id');
            Session::delete('tipo_id');

            parent::prepararViewSite("Site/pag_checkout", $dados);
        }else{
            $msg = 'Atenção! '.$ret['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::voltar();
        }
    }

    function Finalizar()
    {
        $email = filter_input(INPUT_POST, 'email');
        $primeironome = filter_input(INPUT_POST, 'primeironome');
        $segundonome = filter_input(INPUT_POST, 'segundonome');
        $cpf = filter_input(INPUT_POST, 'cpf');
        $telefone = filter_input(INPUT_POST, 'telefone');
        $proposta = filter_input(INPUT_POST, 'proposta');
        $info = filter_input(INPUT_POST, 'info');
        $opentrega = filter_input(INPUT_POST, 'opentrega');

        $endereco = !empty(filter_input(INPUT_POST, 'endereco')) ? filter_input(INPUT_POST, 'endereco') : '';
        $numero = !empty(filter_input(INPUT_POST, 'numero')) ? filter_input(INPUT_POST, 'numero') : 0;
        $complemento = !empty(filter_input(INPUT_POST, 'complemento')) ? filter_input(INPUT_POST, 'complemento') : '';
        $bairro = !empty(filter_input(INPUT_POST, 'bairro')) ? filter_input(INPUT_POST, 'bairro') : '';
        $cep = !empty(filter_input(INPUT_POST, 'cep')) ? filter_input(INPUT_POST, 'cep') : '';
        $uf = !empty(filter_input(INPUT_POST, 'uf')) ? filter_input(INPUT_POST, 'uf') : '';

        $submit = filter_input(INPUT_POST, 'submit');

        if (isset($submit)) {
            $parametros = [
                'PROP_ID' => $proposta,
                'VEN_NOME_COMPLETO' => $primeironome.' '.$segundonome,
                'VEN_EMAIL' => $email,
                'VEN_DOC' => $cpf,
                'VEN_TELEFONE' => $telefone,
                'VEN_OP_ENTREGA' => $opentrega,
                'INFO_ID' => $info,
                'VEN_ENDERECO' => $endereco,
                'VEN_NUMERO' => $numero,
                'VEN_COMPLEMENTO' => $complemento,
                'VEN_BAIRRO' => $bairro,
                'VEN_CEP' => $cep,
                'VEN_UF' => $uf,
            ];

            $retorno = Common::retornoWSLista($this->model->FinalizarProposta($parametros));

            if(intval($retorno['O_COD_RETORNO']) != 0){
                $msg = 'Atenção! '.$retorno['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::voltar();
            }else{
                $msg = 'Venda confirmada com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Site/Finalizada/'.$retorno['O_TOKEN']);
            }
        }
    }

    function Finalizada($token)
    {
        $dados['titulo_1'] = 'Venda finalizada com sucesso';
        $dados['titulo_2'] = 'O código da sua venda é: '.$token;
        $dados['texto_1'] = 'Tenha ele sempre em mãos para agilizar seu atendimento, ';
        $dados['texto_2'] = 'ou entre em contato pelo nosso Whatsapp';
        $dados['texto_3'] = '';

        $dados['footertitulo_1'] = 'Posso confiar no iHelpU? É seguro?';
        $dados['footertitulo_2'] = 'Hoje somos a maior assistência técnica para aparelhos Apple no Rio Grande do Sul.';
        $dados['footertitulo_3'] = 'Possuímos 9 lojas em todo o estado e pelas nossas mãos já passaram mais de 130 mil aparelhos';
        $dados['footertitulo_4'] = 'A maior referência em assistência técnica especializada Apple.';

        $dados['pagina'] = 11;

        parent::prepararViewSite("Site/pag_venda", $dados);
    }

    private function validarCamposObrigatorio()
    {
        $dados['E-mail'] = filter_input(INPUT_POST, 'email');
        $dados['Primeiro Nome'] = filter_input(INPUT_POST, 'primeironome');
        $dados['Último Nome'] = filter_input(INPUT_POST, 'segundonome');
        $dados['CPF'] = filter_input(INPUT_POST, 'cpf');
        $dados['Telefone'] = filter_input(INPUT_POST, 'telefone');
        $dados['Id da Proposta'] = filter_input(INPUT_POST, 'proposta');


        Common::validarInputsObrigatorio($dados, 'Site/Checkout');
    }
}
