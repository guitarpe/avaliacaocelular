<?php

namespace Application\Controller;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Home extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelHome', 'model');

        if (!ini_get('date.timezone')) {
            date_default_timezone_set('GMT');
        }

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = 'Sistema Administrativo ';

        parent::prepararView("Home/pag_home", $dados);
    }

    function Sair()
    {
        Session::destroy();
        Common::redir('Login');
    }

    function MenuClicado()
    {
        $menuclicado = filter_input(INPUT_POST, "id");

        if (!empty($menuclicado)) {
            Session::set("selecionado", $menuclicado);
        }
    }
}
