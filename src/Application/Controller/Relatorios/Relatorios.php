<?php

namespace Application\Controller\Relatorios;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Relatorios extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelRelatorios', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    public function main()
    {
        parent::prepararView("Relatorios/pag_relatorios");
    }

    public function AcessosAdmin()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Relatório de Acessos no Admin";
        $dados['listusuarios'] = Common::retornoWSLista($this->model->ListaRelatorioAcessosAdmin($token), 1);
        parent::prepararView("Relatorios/pag_relatorios_acessos_admin", $dados);
    }

    public function AcessosSite()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Relatório de Acessos do Site";
        $dados['listacessos'] = Common::retornoWSLista($this->model->ListaRelatorioAcessosSite($token), 1);
        parent::prepararView("Relatorios/pag_relatorios_acessos_site", $dados);
    }

    public function Propostas()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Relatório de Propostas Realizadas";
        $dados['listapropostas'] = Common::retornoWSLista($this->model->ListaPropostasRealizadas($token), 1);
        parent::prepararView("Relatorios/pag_relatorios_propostas", $dados);
    }
}
