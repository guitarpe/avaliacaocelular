<?php
namespace Application\Controller;

use avalcelular\Controller;

class Error extends Controller
{

    public function __construct($mensagem = "", $linha = "", $arquivo = "")
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelHome', 'model');

        $dados["erro"] = $mensagem . '<br>LINHA: ' . $linha . '<br>ARQUIVO: ' . $arquivo;
        $dados['exibirlogos'] = true;
        parent::prepararViewSite('error/erro', $dados);
    }
}
