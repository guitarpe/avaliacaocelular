<?php

namespace Application\Controller;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Login extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelUsuario', 'model');
    }

    function main()
    {
        $dados["urlaction"] = SITE_URL . "/Login/Logar";
        parent::prepararViewLogin("Login/pag_login", $dados);
    }

    public function Logar()
    {
        $this->validarCamposObrigatorio(1);

        if (empty(Session::get('token'))) {
            $logar = Common::retornoWSLista($this->model->ValidaLogin(filter_input(INPUT_POST, "login"), filter_input(INPUT_POST, "senha")));
        } else {
            $logar = [
                'O_TOKEN' => Session::get('token'),
                'O_COD_RETORNO' => 0,
                'O_DESC_CURTO' => '',
                'O_TOKEN_INVALIDO' => 'N'
            ];
        }

        if (intval($logar['O_COD_RETORNO']) != 0) {

            if (intval($logar['O_COD_RETORNO']) != 4) {
                Session::set("log-invalido", TRUE);
                Session::set("erro-login", $logar['O_DESC_CURTO']);
                Session::set("erro-class", 'alert-danger pt-2 pb-2');

                $msg = 'Atenção! ' . $logar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Login');
            }else{
                $msg = 'Atenção! ' . $logar['O_DESC_CURTO'];
                $situacao = 'warning';

                Common::alert($msg, $situacao, 'acao');
                Common::redir(URL . SITE_URL . "/Login/Reset/" . Common::encrypt_decrypt('encrypt', $logar['O_US_ID']), 1);
            }
        } else {

            Session::delete("logado");
            Session::set("logado", TRUE);
            Session::set("token", $logar['O_TOKEN']);
            Session::set('selecionado', 1);

            Common::redir('Home');
        }
    }

    function Resetar()
    {
        $dados['urlaction'] = SITE_URL . '/Login/ResertarSenha';
        parent::prepararViewLogin("Login/pag_esqueci_senha", $dados);
    }

    function Reset($code)
    {
        $id = Common::encrypt_decrypt('decrypt', $code);

        $usuario = Common::retornoWSLista($this->model->UsuarioPorID($id));

        $dados['urlaction'] = SITE_URL . '/Login/TrocarSenha';
        $dados['login'] = $usuario["US_LOGIN"];
        $dados['iduser'] = $id;
        $dados['code'] = $code;

        parent::prepararViewLogin("Login/pag_reset_senha", $dados);
    }

    function ResertarSenha()
    {
        $this->validarCamposObrigatorio(2);

        $email = filter_input(INPUT_POST, "email");

        $dados = Common::retornoWSLista($this->model->UsuarioPorEmail($email));
        $config = Common::retornoWSLista($this->model->Configuracoes());

        $parametros = [
            'US_ID' => $dados['US_ID'],
            'US_LOGIN' => $dados['US_LOGIN'],
            'US_SENHA' => '',
            'US_STATUS' => '2'
        ];

        $resetar = Common::retornoWSLista($this->model->TrocarSenhaReset($parametros));

        if ($resetar['O_COD_RETORNO'] != 0) {
            $msg = $resetar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Login/Resetar');
        } else {

            $info = array(
                'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                'fromname' => $config['NOME_CONTATO_DEFAULT'],
                'url' => $config['URL_SISTEMA'],
                'logotipo' => $config['LOGOTIPO']
            );

            $enviado = Common::dispararEmail($dados, $email, 'Reset de Senha', $info, 1);

            if ($enviado) {
                $msg = 'Senha resetada com sucesso!<br/>Você receberá um e-mail para gerar uma nova senha.';
                $situacao = 'success';
            } else {
                $msg = 'Ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                $situacao = 'danger';
            }

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Login');
        }
    }

    function TrocarSenha()
    {
        $this->validarCamposObrigatorio(3);

        $id = filter_input(INPUT_POST, "id");
        $code = filter_input(INPUT_POST, "code");
        $login = filter_input(INPUT_POST, "login");
        $senha = filter_input(INPUT_POST, "senha");
        $confsenha = filter_input(INPUT_POST, "confsenha");

        if ($senha != $confsenha) {
            $msg = 'Você deve confirmar a senha!';
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Login/Reset/' . $code);
        } else {

            $senha = strtoupper(md5($login . $confsenha));

            $parametros = [
                'US_ID' => $id,
                'US_LOGIN' => $login,
                'US_SENHA' => $senha,
                'US_STATUS' => '1'
            ];

            $resetar = Common::retornoWSLista($this->model->TrocarSenhaReset($parametros));

            if ($resetar['O_COD_RETORNO'] != 0) {
                $msg = $resetar['o_ret_msg'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Login/Reset/' . $id);
            } else {
                $msg = 'Senha resetada com sucesso!<br/>Você já pode realizar o login no sistema.';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Login');
            }
        }
    }

    private function validarCamposObrigatorio($tipo)
    {

        if ($tipo == 1) {
            $dados['Login'] = filter_input(INPUT_POST, "login");
            $dados['Senha'] = filter_input(INPUT_POST, "senha");

            Common::validarInputsObrigatorio($dados, 'Login');
        }

        if ($tipo == 2) {
            $dados['E-mail'] = filter_input(INPUT_POST, "email");

            Common::validarInputsObrigatorio($dados, 'Login/Resetar');
        }

        if ($tipo == 3) {
            $dados['Senha'] = filter_input(INPUT_POST, "senha");
            $dados['Confirmar Senha'] = filter_input(INPUT_POST, "confsenha");
            $code = filter_input(INPUT_POST, "code");

            Common::validarInputsObrigatorio($dados, 'Login/Reset/' . $code);
        }
    }
}
