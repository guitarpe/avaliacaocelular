<?php
namespace Application\Controller\Cadastros;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Tipos extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCadastros', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Tipos Cadastrados";
        $dados['listatipos'] = Common::retornoWSLista($this->model->ListaTiposCadastrados($token), 1);
        parent::prepararView("Cadastros/pag_tipos", $dados);
    }

    function Tipo($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Novo Tipo";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Tipos/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Tipo";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Tipos/Editar";
            $dados['tipo'] = Common::retornoWSLista($this->model->DadosTipo($token, $id));
        }

        parent::prepararView("Cadastros/cad_tipos", $dados);
    }

    function Inserir()
    {
        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, 'descricao');

        $this->validarCamposObrigatorio($_FILES['imagem']['name']);

        $pasta = 'tipos';

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['imagem']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 200, 'altura' => 200);

                $ret = parent::salvarImagem($_FILES['imagem'], $pasta, $config);

                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $parametros = [
                'TOKEN' => $token,
                'TP_ID' => 0,
                'TP_DESC' => $descricao,
                'TP_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : null,
                'TP_APELIDO' => Common::removerCaracteresEspeciais($descricao)
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarTipos($parametros));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                $img = implode(',', $imagens);
                if (!empty($img)) {
                    parent::removerImagem($img);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Tipos');
            } else {

                $msg = 'Tipo cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Tipos');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, 'descricao');

        $tipo = Common::retornoWSLista($this->model->DadosTipo($token, $id));

        $imagemantiga = $tipo['TP_IMAGEM'];

        $this->validarCamposObrigatorio($imagemantiga, $id);

        $pasta = 'tipos';

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['imagem']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 200, 'altura' => 200);

                $ret = parent::salvarImagem($_FILES['imagem'], $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $parametros = [
                'TOKEN' => $token,
                'TP_ID' => $id,
                'TP_DESC' => $descricao,
                'TP_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $tipo['TP_IMAGEM'],
                'TP_APELIDO' => Common::removerCaracteresEspeciais($descricao)
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarTipos($parametros));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                if (!empty($imagens[0])) {
                    parent::removerImagem($imagens[0]);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Tipos');
            } else {

                if (!empty($imagens[0])) {
                    parent::removerImagem($imagemantiga);
                }

                $msg = 'Tipo editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Tipos');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'TP_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirTipo($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Tipos';
        } else {
            $tipo = Common::retornoWSLista($this->model->DadosTipo($token, $id));

            $imagemantiga = $tipo['TP_IMAGEM'];
            parent::removerImagem($imagemantiga);
            
            $msg = 'Tipo deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Tipos';
        }
    }

    private function validarCamposObrigatorio($img, $id = null)
    {

        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Tipos/Tipo/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Tipos/Tipo');
        }

        if (empty($img)) {
            $dados['Imagem'] = $_FILES['imagem']['name'];
        }
    }
}
