<?php

namespace Application\Controller\Cadastros;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Capacidades extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCadastros', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Capacidades Cadastradas";
        $dados['listacapacidades'] = Common::retornoWSLista($this->model->ListaCapacidadesCadastrados($token), 1);
        parent::prepararView("Cadastros/pag_capacidades", $dados);
    }

    function Capacidade($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Novo Capacidade";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Capacidades/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Capacidade";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Capacidades/Editar";
            $dados['capacidade'] = Common::retornoWSLista($this->model->DadosCapacidade($token, $id));
        }

        $dados['listamodelos'] = Common::retornoWSLista($this->model->ListaModelosCadastrados($token), 1);
        $dados['listatipos'] = Common::retornoWSLista($this->model->ListaTiposCadastrados($token), 1);

        parent::prepararView("Cadastros/cad_capacidades", $dados);
    }

    function GetModelos(){
        $tipo = filter_input(INPUT_POST, 'tipo');

        echo json_encode(Common::retornoWSLista($this->model->ListaModelosPorTipo($tipo), 1));
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $valadicional = filter_input(INPUT_POST, 'valadicional');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'CAP_ID' => 0,
                'CAP_DESC' => $descricao,
                'CAP_VAL_AD' => empty($valadicional) ? 0 : Common::returnValor($valadicional),
                'CAP_APELIDO' => Common::removerCaracteresEspeciais($descricao)
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarCapacidades($parametros));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Capacidades');
            } else {

                $msg = 'Capacidade cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Capacidades');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $valadicional = filter_input(INPUT_POST, 'valadicional');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'CAP_ID' => $id,
                'CAP_DESC' => $descricao,
                'CAP_VAL_AD' => empty($valadicional) ? 0 : Common::returnValor($valadicional),
                'CAP_APELIDO' => Common::removerCaracteresEspeciais($descricao)
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarCapacidades($parametros));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Capacidades');
            } else {

                $msg = 'Capacidade editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Capacidades');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'CAP_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirCapacidade($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Capacidades';
        } else {
            $msg = 'Capacidade deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Capacidades';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Modelo do Aparelho'] = filter_input(INPUT_POST, 'modelo');
        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        //$dados['Valor Adicional'] = filter_input(INPUT_POST, 'valadicional');

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Capacidades/Capacidade/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Capacidades/Capacidade');
        }
    }
}
