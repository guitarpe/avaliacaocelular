<?php

namespace Application\Controller\Cadastros;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Lojas extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCadastros', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Lojas Cadastradas";
        $dados['listalojas'] = Common::retornoWSLista($this->model->ListaLojasCadastrados($token), 1);

        parent::prepararView("Cadastros/pag_lojas", $dados);
    }

    function Loja($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Nova Loja";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Lojas/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Loja";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Lojas/Editar";
            $dados['loja'] = Common::retornoWSLista($this->model->DadosLoja($token, $id));
        }

        parent::prepararView("Cadastros/cad_lojas", $dados);
    }

    function Inserir()
    {
        self::validarCamposObrigatorio(1);

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $razao_social = filter_input(INPUT_POST, "razao_social");
        $endereco_completo = filter_input(INPUT_POST, "endereco_completo");
        $complemento = filter_input(INPUT_POST, "complemento");
        $cep = filter_input(INPUT_POST, "cep");
        $telefone = filter_input(INPUT_POST, "telefone");
        $atend_semana = filter_input(INPUT_POST, "atend_semana");
        $atend_fim_semana = filter_input(INPUT_POST, "atend_fim_semana");
        $atend_feriado = filter_input(INPUT_POST, "atend_feriado");

        if (isset($submit)) {
            $parametros = [
                'TOKEN' => $token,
                'LOJ_ID' => 0,
                'LOJ_RAZAO_SOCIAL' => $razao_social,
                'LOJ_ENDERECO_COMPL' => $endereco_completo,
                'LOJ_COMPLEMENTO' => $complemento ,
                'LOJ_CEP' => $cep,
                'LOJ_TELEFONE' => $telefone,
                'LOJ_ATENDIMENTO_SEM' => $atend_semana,
                'LOJ_ATENDIMENTO_FS' => $atend_fim_semana,
                'LOJ_ATENDIMENTO_FERIADO' => $atend_feriado
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarLojas($parametros));

            if (intval($cadastrar['O_COD_RETORNO']) != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Lojas');
            } else {

                $msg = 'Loja cadastrada com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Lojas');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        self::validarCamposObrigatorio(2);

        $id = filter_input(INPUT_POST, "id");
        $submit = filter_input(INPUT_POST, "submit");
        $razao_social = filter_input(INPUT_POST, "razao_social");
        $endereco_completo = filter_input(INPUT_POST, "endereco_completo");
        $complemento = filter_input(INPUT_POST, "complemento");
        $cep = filter_input(INPUT_POST, "cep");
        $telefone = filter_input(INPUT_POST, "telefone");
        $atend_semana = filter_input(INPUT_POST, "atend_semana");
        $atend_fim_semana = filter_input(INPUT_POST, "atend_fim_semana");
        $atend_feriado = filter_input(INPUT_POST, "atend_feriado");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'LOJ_ID' => $id,
                'LOJ_RAZAO_SOCIAL' => $razao_social,
                'LOJ_ENDERECO_COMPL' => $endereco_completo,
                'LOJ_COMPLEMENTO' => $complemento ,
                'LOJ_CEP' => $cep,
                'LOJ_TELEFONE' => $telefone,
                'LOJ_ATENDIMENTO_SEM' => $atend_semana,
                'LOJ_ATENDIMENTO_FS' => $atend_fim_semana,
                'LOJ_ATENDIMENTO_FERIADO' => $atend_feriado
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarLojas($dados));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Lojas/Loja/' . $id);
            } else {

                $msg = 'Loja Editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Lojas');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");

        $token = Session::get('token');
        $loja = Common::retornoWSLista($this->model->DadosLoja($token, $id));

        $dados = [
            'TOKEN' => $token,
            'LOJ_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirLoja($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = 'Erro ao deletar o Loja, tente novamente mais tarde! ' . $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Lojas';
        } else {
            $msg = 'Loja deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Lojas';
        }
    }

    private function validarCamposObrigatorio($tipo)
    {
        if ($tipo == 1) {

            $dados['Razão Social'] = filter_input(INPUT_POST, "razao_social");
            $dados['Endereço Completo'] = filter_input(INPUT_POST, "endereco_completo");
            $dados['CEP'] = filter_input(INPUT_POST, "cep");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['Atendimento Semanal'] = filter_input(INPUT_POST, "atend_semana");
            //$dados['Atendimento Fim de Semana'] = filter_input(INPUT_POST, "atend_fim_semana");
            //$dados['Atendimento Feriados'] = filter_input(INPUT_POST, "atend_feriado");

            Common::validarInputsObrigatorio($dados, 'Lojas/Loja');
        }

        if ($tipo == 2) {

            $id = filter_input(INPUT_POST, "id");
            $dados['Razão Social'] = filter_input(INPUT_POST, "razao_social");
            $dados['Endereço Completo'] = filter_input(INPUT_POST, "endereco_completo");
            $dados['CEP'] = filter_input(INPUT_POST, "cep");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['Atendimento Semanal'] = filter_input(INPUT_POST, "atend_semana");
            //$dados['Atendimento Fim de Semana'] = filter_input(INPUT_POST, "atend_fim_semana");
            //$dados['Atendimento Feriados'] = filter_input(INPUT_POST, "atend_feriado");

            Common::validarInputsObrigatorio($dados, 'Lojas/Loja/' . $id);
        }
    }
}
