<?php

namespace Application\Controller\Cadastros;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Redes extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCadastros', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Redes Cadastradas";
        $dados['listaredes'] = Common::retornoWSLista($this->model->ListaRedesCadastradas($token), 1);

        parent::prepararView("Cadastros/pag_redes", $dados);
    }

    function Rede($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Nova Rede";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Redes/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Rede";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Redes/Editar";
            
            $dados['rede'] = Common::retornoWSLista($this->model->DadosRede($token, $id));
        }

        $dados['listamodelos'] = Common::retornoWSLista($this->model->ListaModelosCadastrados($token), 1);
        $dados['listatipos'] = Common::retornoWSLista($this->model->ListaTiposCadastrados($token), 1);

        parent::prepararView("Cadastros/cad_redes", $dados);
    }

    function GetModelos(){
        $tipo = filter_input(INPUT_POST, 'tipo');

        echo json_encode(Common::retornoWSLista($this->model->ListaModelosPorTipo($tipo), 1));
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $valadicional = filter_input(INPUT_POST, 'valadicional');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'RED_ID' => 0,
                'RED_DESC' => $descricao,
                'RED_APELIDO' => Common::removerCaracteresEspeciais($descricao),
				'RED_VAL_AD' => empty($valadicional) ? 0 : Common::returnValor($valadicional)
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarRedes($parametros));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            } else {

                $msg = 'Rede cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $valadicional = filter_input(INPUT_POST, 'valadicional');
        
        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'RED_ID' => $id,
                'RED_DESC' => $descricao,
                'RED_APELIDO' => Common::removerCaracteresEspeciais($descricao),
				'RED_VAL_AD' => empty($valadicional) ? 0 : Common::returnValor($valadicional)
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarRedes($parametros));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            } else {

                $msg = 'Rede editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Redes');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'RED_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirRede($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Redes';
        } else {
            $msg = 'Rede deletada com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Redes';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Modelo do Aparelho'] = filter_input(INPUT_POST, 'modelo');
        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        $dados['Valor Adicional'] = filter_input(INPUT_POST, 'valadicional');
        
        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Redes/Rede/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Redes/Rede');
        }
    }
}
