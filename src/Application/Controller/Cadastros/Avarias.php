<?php

namespace Application\Controller\Cadastros;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Avarias extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCadastros', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Avarias Cadastradas";
        $dados['listaavarias'] = Common::retornoWSLista($this->model->ListaAvariasCadastrados($token), 1);
        parent::prepararView("Cadastros/pag_avarias", $dados);
    }

    function Avaria($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Nova Avaria";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Avarias/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Avaria";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Avarias/Editar";
            $dados['avaria'] = Common::retornoWSLista($this->model->DadosAvaria($token, $id));
        }

        $dados['listamodelos'] = Common::retornoWSLista($this->model->ListaModelosCadastrados($token), 1);
        $dados['listatipos'] = Common::retornoWSLista($this->model->ListaTiposCadastrados($token), 1);

        parent::prepararView("Cadastros/cad_avarias", $dados);
    }

    function GetModelos(){
        $tipo = filter_input(INPUT_POST, 'tipo');

        echo json_encode(Common::retornoWSLista($this->model->ListaModelosPorTipo($tipo), 1));
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $desconto = filter_input(INPUT_POST, 'desconto');
        $op_outro = filter_input(INPUT_POST, 'op_outro');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'AVA_ID' => 0,
                'AVA_DESC' => $descricao,
                'AVA_VAL_DESC' => empty($desconto) ? 0 : Common::returnValor($desconto),
                'AVA_OP_OUTRO' => $op_outro
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarAvarias($parametros));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Avarias');
            } else {

                $msg = 'Avaria cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Avarias');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $desconto = filter_input(INPUT_POST, 'desconto');
        $op_outro = filter_input(INPUT_POST, 'op_outro');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'AVA_ID' => $id,
                'AVA_DESC' => $descricao,
                'AVA_VAL_DESC' => empty($desconto) ? 0 : Common::returnValor($desconto),
                'AVA_OP_OUTRO' => $op_outro
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarAvarias($parametros));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Avarias');
            } else {

                $msg = 'Avaria editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Avarias');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'AVA_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirAvaria($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Avarias';
        } else {
            $msg = 'Avaria deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Avarias';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Modelo'] = filter_input(INPUT_POST, 'modelo');
        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        $dados['Desconto'] = filter_input(INPUT_POST, 'desconto');

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Avarias/Avaria/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Avarias/Avaria');
        }
    }
}
