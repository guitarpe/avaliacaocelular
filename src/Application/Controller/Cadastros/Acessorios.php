<?php

namespace Application\Controller\Cadastros;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Acessorios extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCadastros', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Acessorios Cadastrados";
        $dados['listaaccessorios'] = Common::retornoWSLista($this->model->ListaAcessoriosCadastrados($token), 1);
        parent::prepararView("Cadastros/pag_acessorios", $dados);
    }

    function Acessorio($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Novo Acessorio";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Acessorios/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Acessorio";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Acessorios/Editar";
            $dados['acessorio'] = Common::retornoWSLista($this->model->DadosAcessorio($token, $id));
        }

        $dados['listamodelos'] = Common::retornoWSLista($this->model->ListaModelosCadastrados($token), 1);
        $dados['listatipos'] = Common::retornoWSLista($this->model->ListaTiposCadastrados($token), 1);

        parent::prepararView("Cadastros/cad_acessorios", $dados);
    }

    function GetModelos(){
        $tipo = filter_input(INPUT_POST, 'tipo');

        echo json_encode(Common::retornoWSLista($this->model->ListaModelosPorTipo($tipo), 1));
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $valadicional = filter_input(INPUT_POST, 'valadicional');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'ACS_ID' => 0,
                'ACS_DESC' => $descricao,
                'ACS_VAL_AD' => empty($valadicional) ? 0 : Common::returnValor($valadicional)
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarAcessorios($parametros));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Acessorios');
            } else {

                $msg = 'Acessorio cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Acessorios');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $modelo = filter_input(INPUT_POST, 'modelo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $valadicional = filter_input(INPUT_POST, 'valadicional');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'MOD_ID' => $modelo,
                'ACS_ID' => $id,
                'ACS_DESC' => $descricao,
                'ACS_VAL_AD' => empty($valadicional) ? 0 : Common::returnValor($valadicional)
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarAcessorios($parametros));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Acessorios');
            } else {

                $msg = 'Acessorio editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Acessorios');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'ACS_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirAcessorio($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Acessorios';
        } else {
            $msg = 'Acessorio deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Acessorios';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Modelo'] = filter_input(INPUT_POST, 'modelo');
        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        $dados['Valor Adicional'] = filter_input(INPUT_POST, 'valadicional');

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Acessorios/Acessorio/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Acessorios/Acessorio');
        }
    }
}
