<?php

namespace Application\Controller\Cadastros;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Modelos extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelCadastros', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Modelos Cadastrados";
        $dados['listamodelos'] = Common::retornoWSLista($this->model->ListaModelosCadastrados($token), 1);
        parent::prepararView("Cadastros/pag_modelos", $dados);
    }

    function Modelo($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Cadastrar Novo Modelo";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Modelos/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Modelo";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Modelos/Editar";
            
            $dados['modelo'] = Common::retornoWSLista($this->model->DadosModelo($token, $id));
        }

        $dados['listatipos'] = Common::retornoWSLista($this->model->ListaTiposCadastrados($token), 1);

        parent::prepararView("Cadastros/cad_modelos", $dados);
    }

    function Inserir()
    {
        $this->validarCamposObrigatorio();

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $tipo = filter_input(INPUT_POST, 'tipo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $precovenda = filter_input(INPUT_POST, 'precovenda');
        $precocompra = filter_input(INPUT_POST, 'precocompra');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'TP_ID' => $tipo,
                'MOD_ID' => 0,
                'MOD_DESC' => $descricao,
                'MOD_PRC_VENDA' => empty($precovenda) ? 0 : Common::returnValor($precovenda),
                'MOD_PRC_COMPRA' => empty($precocompra) ? 0 : Common::returnValor($precocompra),
                'MOD_APELIDO' => Common::removerCaracteresEspeciais($descricao)
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarModelos($parametros));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modelos');
            } else {

                $msg = 'Modelo cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modelos');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");

        $this->validarCamposObrigatorio($id);

        $submit = filter_input(INPUT_POST, "submit");
        $tipo = filter_input(INPUT_POST, 'tipo');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $precovenda = filter_input(INPUT_POST, 'precovenda');
        $precocompra = filter_input(INPUT_POST, 'precocompra');

        if (isset($submit)) {

            $parametros = [
                'TOKEN' => $token,
                'TP_ID' => $tipo,
                'MOD_ID' => $id,
                'MOD_DESC' => $descricao,
                'MOD_PRC_VENDA' => empty($precovenda) ? 0 : Common::returnValor($precovenda),
                'MOD_PRC_COMPRA' => empty($precocompra) ? 0 : Common::returnValor($precocompra),
                'MOD_APELIDO' => Common::removerCaracteresEspeciais($descricao)
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarModelos($parametros));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modelos');
            } else {

                $msg = 'Modelo editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Modelos');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'MOD_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirModelo($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Modelos';
        } else {
            $msg = 'Modelo deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Modelos';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {

        $dados['Tipo'] = filter_input(INPUT_POST, 'tipo');
        $dados['Descrição'] = filter_input(INPUT_POST, 'descricao');
        $dados['Preço de Venda'] = filter_input(INPUT_POST, 'precovenda');
        $dados['Preço de Compra'] = filter_input(INPUT_POST, 'precocompra');

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Modelos/Modelo/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Modelos/Modelo');
        }
    }
}
