<?php

namespace Application\Controller\Config;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Sistema extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');
        $tabativa = Session::get('tabativa');

        $dados['urlactioncontato'] = SITE_URL . "/Sistema/SalvarContato";
        $dados['urlactionsmtp'] = SITE_URL . "/Sistema/SalvarInfoSMTP";
        $dados['urlactionimg'] = SITE_URL . "/Sistema/SalvarImagens";
        $dados['cfgcontato'] = Common::retornoWSLista($this->model->ConfiguracoesADM($token));
        $dados['titulopagina'] = "Configurações e Informações Gerais do Sistema";
        $dados['titleaction'] = 'Salvar';
        $dados['tabativa'] = empty($tabativa) ? 1 : $tabativa;

        parent::prepararView("Config/pag_config", $dados);
    }

    function SalvarContato()
    {
        $token = Session::get('token');

        self::validarCamposObrigatorio(1);

        $submit = filter_input(INPUT_POST, "submit");

        $idcontato = filter_input(INPUT_POST, "idcontato");
        $endereco = filter_input(INPUT_POST, "endereco");
        $numero = filter_input(INPUT_POST, "numero");
        $complemento = filter_input(INPUT_POST, "complemento");
        $bairro = filter_input(INPUT_POST, "bairro");
        $cidade = filter_input(INPUT_POST, "cidade");
        $uf = filter_input(INPUT_POST, "uf");
        $telefone = filter_input(INPUT_POST, "telefone");
        $cep = filter_input(INPUT_POST, "cep");
        $email = filter_input(INPUT_POST, "email");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idcontato,
                'ENDERECO' => $endereco,
                'BAIRRO' => $bairro,
                'NUMERO' => $numero,
                'CIDADE' => $cidade,
                'UF' => $uf,
                'CEP' => $cep,
                'COMPLEMENTO' => $complemento,
                'TELEFONE' => $telefone,
                'EMAIL_TECNICO' => $email
            ];

            $cadastrar = Common::retornoWSLista($this->model->SalvarSisInfoContato($dados));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Session::set('tabativa', 1);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Informações de contato salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 1);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    function SalvarInfoSMTP()
    {
        self::validarCamposObrigatorio(2);

        $token = Session::get('token');

        $idsmtp = filter_input(INPUT_POST, "idsmtp");
        $host = filter_input(INPUT_POST, "host");
        $usuario = filter_input(INPUT_POST, "usuario");
        $senha = filter_input(INPUT_POST, "senha");
        $porta = filter_input(INPUT_POST, "porta");
        $contatopadrao = filter_input(INPUT_POST, "contatopadrao");
        $emailcontatopadrao = filter_input(INPUT_POST, "emailcontatopadrao");
        $submit = filter_input(INPUT_POST, "submit");

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idsmtp,
                'SMTP_HOST' => $host,
                'SMTP_PORTA' => $porta,
                'SMTP_USER' => $usuario,
                'SMTP_SENHA' => $senha,
                'MAIL_CONTATO_DEFAULT' => $emailcontatopadrao,
                'NOME_CONTATO_DEFAULT' => $contatopadrao,
            ];

            $cadastrar = Common::retornoWSLista($this->model->SalvarSisInfoSMTP($dados));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Session::set('tabativa', 3);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Informações salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 3);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    function SalvarImagens()
    {
        $token = Session::get('token');

        $submit = filter_input(INPUT_POST, "submit");
        $idimagens = filter_input(INPUT_POST, "idimagens");

        $cfgcontato = Common::retornoWSLista($this->model->ConfiguracoesADM($token));

        $logotipo = [];
        $favicon = [];

        if (!empty($_FILES['logotipo']['name'])) {
            $config = array('tamanho' => 4999999, 'largura' => 250, 'altura' => 200);
            $ret = Common::retornoWSLista(parent::salvarImagem($_FILES['logotipo'], '', $config), 1);

            array_push($logotipo,  $ret['imagem']);
        }

        if (!empty($_FILES['favicon']['name'])) {
            $config = array('tamanho' => 4999999, 'largura' => 250, 'altura' => 200);
            $ret = Common::retornoWSLista(parent::salvarImagem($_FILES['favicon'], '', $config), 1);
            array_push($favicon, $ret['imagem']);
        }

        self::validarCamposObrigatorioImg($cfgcontato['LOGOTIPO'], $cfgcontato['FAVICON']);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'ID' => $idimagens,
                'LOGOTIPO' => count($logotipo) > 0 ? implode(',', $logotipo) : $cfgcontato['LOGOTIPO'],
                'FAVICON' => count($favicon) > 0 ? implode(',', $favicon) : $cfgcontato['FAVICON']
            ];

            $cadastrar = Common::retornoWSLista($this->model->SalvarSisImagens($dados));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                $imgl = implode(',', $logotipo);
                if (!empty($imgl)) {
                    parent::removerImagem($imgl);
                }

                $imgf = implode(',', $favicon);
                if (!empty($imgf)) {
                    parent::removerImagem($imgf);
                }

                Session::set('tabativa', 4);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            } else {
                $msg = 'Imagens salvas com sucesso!';
                $situacao = 'success';

                Session::set('tabativa', 4);
                Common::alert($msg, $situacao, 'acao');
                Common::redir('Sistema');
            }
        }
    }

    private function validarCamposObrigatorio($tipo = null, $op = null)
    {
        if ($tipo == 1) {
            $dados['Endereço'] = filter_input(INPUT_POST, "endereco");
            $dados['Número'] = filter_input(INPUT_POST, "numero");
            $dados['Complemento'] = filter_input(INPUT_POST, "complemento");
            $dados['Bairro'] = filter_input(INPUT_POST, "bairro");
            $dados['Cidade'] = filter_input(INPUT_POST, "cidade");
            $dados['UF'] = filter_input(INPUT_POST, "uf");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['CEP'] = filter_input(INPUT_POST, "cep");
            $dados['E-mail'] = filter_input(INPUT_POST, "email");
        }

        if ($tipo == 2) {
            $dados['Host'] = filter_input(INPUT_POST, "host");
            $dados['Usuário'] = filter_input(INPUT_POST, "usuario");
            $dados['Senha'] = filter_input(INPUT_POST, "senha");
            $dados['Porta'] = filter_input(INPUT_POST, "porta");
            $dados['Contato Padrão'] = filter_input(INPUT_POST, "contatopadrao");
            $dados['Email do Contato Padrão'] = filter_input(INPUT_POST, "emailcontatopadrao");
        }

        Common::validarInputsObrigatorio($dados, 'Sistema');
    }

    private function validarCamposObrigatorioImg($logo, $favicon)
    {
        if (empty($logo)) {
            $dados['Logotipo'] = $_FILES['logotipo']['name'];
        }

        if (empty($favicon)) {
            $dados['Favicon'] = $_FILES['favicon']['name'];
        }

        Common::validarInputsObrigatorio($dados, 'Sistema');
    }
}
