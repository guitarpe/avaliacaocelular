<?php

namespace Application\Controller\Config;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session,
    avalcelular\PHPMailer;

class Usuarios extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelUsuario', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Usuários Cadastrados";
        $dados['listausuarios'] = Common::retornoWSLista($this->model->ListaUsuariosCadastrados($token), 1);

        parent::prepararView("Config/pag_usuarios", $dados);
    }

    function Usuario($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (empty($id)) {
            $dados['titulopagina'] = "Novo Usuário";
            $dados['titleaction'] = "Cadastrar";
            $dados['urlaction'] = SITE_URL . "/Usuarios/Inserir";
        } else {
            $dados['titulopagina'] = "Editar Usuário";
            $dados['titleaction'] = "Salvar Edição";
            $dados['urlaction'] = SITE_URL . "/Usuarios/Editar";
            $dados['usuario'] = Common::retornoWSLista($this->model->DadosUsuario($token, $id));
        }
        $dados['listaperfis'] = Common::retornoWSLista($this->model->ListaPerfisCadastrados($token), 1);

        parent::prepararView("Config/cad_usuarios", $dados);
    }

    function ResertarSenha($id)
    {
        $token = Session::get('token');

        $dados = Common::retornoWSLista($this->model->DadosUsuario($token, $id));
        $config = Common::retornoWSLista($this->model->Configuracoes($token));

        $parametros = [
            'US_ID' => $id,
            'US_LOGIN' => $dados['US_LOGIN'],
            'US_SENHA' => '',
            'US_STATUS' => '2'
        ];

        $resetar = Common::retornoWSLista($this->model->TrocarSenhaReset($parametros));

        if ($resetar['O_COD_RETORNO'] != 0) {
            $msg = $resetar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios');
        } else {

            $info = array(
                'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                'fromname' => $config['NOME_CONTATO_DEFAULT'],
                'url' => $config['URL_SISTEMA'],
                'logotipo' => $config['LOGOTIPO']
            );

            $enviado = Common::dispararEmail($dados, $dados['EMAIL'], 'Reset de Senha', $info, 1);

            if ($enviado) {
                $msg = 'Senha resetada com sucesso!<br/>O usuário receberá um e-mail para gerar uma nova senha.';
                $situacao = 'success';
            } else {
                $msg = 'Ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                $situacao = 'danger';
            }

            Common::alert($msg, $situacao, 'acao');
            Common::redir('Usuarios');
        }
    }

    function Inativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'US_ID' => $id,
            'US_STATUS' => 0
        ];

        $status = Common::retornoWSLista($this->model->AtivarInativarUsuario($parametros));

        if ($status['O_COD_RETORNO'] != 0) {
            $msg = $status['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Inativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function Ativar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'US_ID' => $id,
            'US_STATUS' => 1
        ];

        $status = Common::retornoWSLista($this->model->AtivarInativarUsuario($parametros));

        if ($status['O_COD_RETORNO'] != 0) {
            $msg = $status['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Ativado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }

    function MeusDados()
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        $dados['titulopagina'] = "Meus Dados";
        $dados['titleaction'] = "Salvar Edição";
        $dados['urlaction'] = SITE_URL . "/Usuarios/Editar";
        $dados['usuario'] = Common::retornoWSLista($this->model->UsuarioLogado($token));
        parent::prepararView("Config/cad_usuarios", $dados);
    }

    function Inserir()
    {
        self::validarCamposObrigatorio(1);

        $token = Session::get('token');
        $submit = filter_input(INPUT_POST, "submit");
        $perfil = filter_input(INPUT_POST, "perfil");
        $nome = filter_input(INPUT_POST, "nome");
        $email = filter_input(INPUT_POST, "email");
        $telefone = filter_input(INPUT_POST, "telefone");
        $genero = filter_input(INPUT_POST, "genero");
        $login = filter_input(INPUT_POST, "login");

        $now = new \DateTime();

        $pasta = 'fotosusuarios/'.$now->format('Y_m_d_H_i_s');

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 200, 'altura' => 200);

                $ret = parent::salvarImagem($_FILES['foto'], $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $parametros = [
                'TOKEN' => $token,
                'US_ID' => 0,
                'PER_ID' => $perfil,
                'US_LOGIN' => $login,
                'US_PASS' => strtoupper(md5($login . 'mudar123')),
                'US_STATUS' => '2',
                'US_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : null,
                'NOME_USER' => $nome,
                'EMAIL_USER' => $email,
                'GENERO_USER' => $genero,
                'TEL_USER' => $telefone,
                'PASTA_USER' => $pasta
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarUsuarios($parametros));

            if (intval($cadastrar['O_COD_RETORNO']) != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                $img = implode(',', $imagens);
                if (!empty($img)) {
                    parent::removerImagem($img);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            } else {
                $dados = Common::retornoWSLista($this->model->UsuarioPorEmail($email));
                $config = Common::retornoWSLista($this->model->Configuracoes($token));

                $info = array(
                    'host' => Common::encrypt_decrypt('decrypt', $config['SMTP_HOST']),
                    'porta' => Common::encrypt_decrypt('decrypt', $config['SMTP_PORTA']),
                    'username' => Common::encrypt_decrypt('decrypt', $config['SMTP_USER']),
                    'senha' => Common::encrypt_decrypt('decrypt', $config['SMTP_SENHA']),
                    'from' => Common::encrypt_decrypt('decrypt', $config['MAIL_CONTATO_DEFAULT']),
                    'fromname' => $config['NOME_CONTATO_DEFAULT'],
                    'url' => $config['URL_SISTEMA'],
                    'logotipo' => $config['LOGOTIPO']
                );

                $enviado = Common::dispararEmail($dados, $email, 'Novo Usuário Cadastrado', $info, 2);

                if ($enviado) {
                    $msg = 'Usuário cadastrado com sucesso!<br/>O usuário receberá um e-mail para gerar uma nova senha.';
                    $situacao = 'success';
                } else {
                    $msg = 'Usuário cadastrado com sucesso porém ocorreu um erro ao enviar o e-mail!<br/>Entre em contato com o administrador.';
                    $situacao = 'warning';
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            }
        }
    }

    function RetornoLoginUsuario(){
        $nome = filter_input(INPUT_POST, "nome");
        echo Common::retornoLoginGerado($nome);
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');
        $usuario = Common::retornoWSLista($this->model->DadosUsuario($token, $id));

        $submit = filter_input(INPUT_POST, "submit");

        $perfil = filter_input(INPUT_POST, "perfil");
        $nome = filter_input(INPUT_POST, "nome");
        $email = filter_input(INPUT_POST, "email");
        $telefone = filter_input(INPUT_POST, "telefone");
        $genero = filter_input(INPUT_POST, "genero");
        $login = filter_input(INPUT_POST, "login");
        $status = filter_input(INPUT_POST, "status");

        $confsenha = filter_input(INPUT_POST, "confsenha");

        self::validarCamposObrigatorio(2, $usuario['US_IMAGEM']);

        $now = new \DateTime();

        $imagemantiga = $usuario['US_IMAGEM'];

        $pasta = empty($usuario['PASTA_USER']) ? 'fotosusuarios/'.$now->format('Y_m_d_H_i_s') : $usuario['PASTA_USER'];

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 200, 'altura' => 200);

                $ret = parent::salvarImagem($_FILES['foto'], $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['list']['imagem']);
            }

            $dados = [
                'TOKEN' => $token,
                'US_ID' => $id,
                'PER_ID' => $perfil,
                'US_LOGIN' => $login,
                'US_PASS' => empty($confsenha) ? $usuario['US_SENHA'] : strtoupper(md5($login . $confsenha)),
                'US_STATUS' => $status,
                'US_IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $usuario['US_IMAGEM'],
                'NOME_USER' => $nome,
                'EMAIL_USER' => $email,
                'GENERO_USER' => $genero,
                'TEL_USER' => $telefone,
                'PASTA_USER' => $pasta
            ];

            $editar = Common::retornoWSLista($this->model->CadastrarEditarUsuarios($dados));

            if ($editar['O_COD_RETORNO'] != 0) {
                $msg = $editar['O_DESC_CURTO'];
                $situacao = 'danger';

                if (!empty($imagens[0])) {
                    parent::removerImagem($imagens[0]);
                }

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios/Usuario/' . $id);
            } else {

                if (!empty($imagens[0])) {
                    parent::removerImagem($imagemantiga);
                }

                $msg = 'Usuário Editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Usuarios');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");

        $token = Session::get('token');
        $usuario = Common::retornoWSLista($this->model->DadosUsuario($token, $id));

        $dados = [
            'TOKEN' => $token,
            'US_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirUsuario($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = 'Erro ao deletar o usuário, tente novamente mais tarde! ' . $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Usuarios';
        } else {
            $msg = 'Usuário deletado com sucesso!';
            $situacao = 'success';

            parent::removerImagem($usuario['US_IMAGEM']);

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Usuarios';
        }
    }

    private function validarCamposObrigatorio($tipo, $img = null)
    {
        if ($tipo == 1) {

            $dados['Perfil de Usuário'] = filter_input(INPUT_POST, "perfil");
            $dados['Nome Completo'] = filter_input(INPUT_POST, "nome");
            $dados['Sexo'] = filter_input(INPUT_POST, "genero");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['Login'] = filter_input(INPUT_POST, "login");
            $dados['E-mail'] = filter_input(INPUT_POST, "email");

            //$dados['Foto'] = $_FILES['foto']['name'];

            Common::validarInputsObrigatorio($dados, 'Usuarios/Usuario');
        }

        if ($tipo == 2) {

            $id = filter_input(INPUT_POST, "id");
            $dados['Perfil de Usuário'] = filter_input(INPUT_POST, "perfil");
            $dados['Nome Completo'] = filter_input(INPUT_POST, "nome");
            $dados['Sexo'] = filter_input(INPUT_POST, "genero");
            $dados['Telefone'] = filter_input(INPUT_POST, "telefone");
            $dados['Login'] = filter_input(INPUT_POST, "login");
            $dados['E-mail'] = filter_input(INPUT_POST, "email");
            $dados['Status'] = filter_input(INPUT_POST, "status");

            //if (empty($img)) {
            //    $dados['Foto'] = $_FILES['foto']['name'];
            //}

            Common::validarInputsObrigatorio($dados, 'Usuarios/Usuario/' . $id);
        }
    }
}
