<?php

namespace Application\Controller\Config;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Coletas extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Coletas Cadastradas";
        $dados['listacoletas'] = Common::retornoWSLista($this->model->ListaColetasCadastrados($token), 1);
        parent::prepararView("Config/pag_coletas", $dados);
    }

    function Coleta($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['coleta'] = Common::retornoWSLista($this->model->DadosColeta($token, $id));
            $dados['urlaction'] = SITE_URL . "/Coletas/Editar";
            $dados['titulopagina'] = "Editar Coleta";    
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Coletas/Inserir";
            $dados['titulopagina'] = "Nova Coleta";
        }
        
        $dados['listameios'] = Common::retornoWSLista($this->model->ListaMeiosEntregaCadastrados($token), 1);

        parent::prepararView("Config/cad_coleta", $dados);
    }

    function Inserir()
    {   
        $token = Session::get('token');

        $this->validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $valor_cobrado = filter_input(INPUT_POST, "valor_cobrado");
        $modelo_entrega = filter_input(INPUT_POST, "modelo_entrega");
        $status = filter_input(INPUT_POST, "status");

        if (isset($submit)) {
            $dados = [
                'TOKEN' => $token,
                'CLT_ID' => 0,
                'CLT_DESC' => $descricao,
                'CLT_VAL_DESC' => empty($valor_cobrado) ? 0 : Common::returnValor($valor_cobrado),
                'MEIO_ID' => intval($modelo_entrega),
                'CLT_STATUS' => $status
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarColetas($dados));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Coletas');
            } else {
                $msg = 'Coleta cadastrada com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Coletas');
            }
        }
    }

    function Editar()
    {
        $token = Session::get('token');

        $id = filter_input(INPUT_POST, "id");
        $submit = filter_input(INPUT_POST, "submit");
        $descricao = filter_input(INPUT_POST, "descricao");
        $valor_cobrado = filter_input(INPUT_POST, "valor_cobrado");
        $modelo_entrega = filter_input(INPUT_POST, "modelo_entrega");
        $status = filter_input(INPUT_POST, "status");

        $this->validarCamposObrigatorio($id);

        if (isset($submit)) {

            $dados = [
                'TOKEN' => $token,
                'CLT_ID' => $id,
                'CLT_DESC' => $descricao,
                'CLT_VAL_DESC' => empty($valor_cobrado) ? 0 : Common::returnValor($valor_cobrado),
                'MEIO_ID' => intval($modelo_entrega),
                'CLT_STATUS' => $status
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarColetas($dados));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Coletas');
            } else {
                $msg = 'Coleta editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Coletas');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'CLT_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirColeta($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Coletas';
        } else {
            $msg = 'Coleta deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Coletas';
        }
    }

    private function validarCamposObrigatorio($id = null)
    {
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");
        //$dados['Valor Cobrado'] = filter_input(INPUT_POST, "valor_cobrado");
        $dados['Modelo de Entrega'] = filter_input(INPUT_POST, "modelo_entrega");

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Coletas/Coleta/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Coletas/Coleta');
        }
    }
}
