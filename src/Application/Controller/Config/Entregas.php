<?php

namespace Application\Controller\Config;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Entregas extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelConfig', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Meios de Entregas Cadastrados";
        $dados['listameios'] = Common::retornoWSLista($this->model->ListaMeiosEntregaCadastrados($token), 1);
        parent::prepararView("Config/pag_meios_entregas", $dados);
    }

    function MeioEntrega($id = null)
    {
        $token = Session::get('token');

        $dados = parent::carregarMenu();

        if (!empty($id)) {
            $dados['titleaction'] = 'Salvar Edição';
            $dados['meioentrega'] = Common::retornoWSLista($this->model->DadosMeioEntrega($token, $id));
            $dados['urlaction'] = SITE_URL . "/Entregas/Editar";
            $dados['titulopagina'] = "Editar Entrega";
            $dados['listacidades'] = Common::retornoWSLista($this->model->ListaCidadesAbrangencia($token, $id), 1);
        } else {
            $dados['titleaction'] = 'Cadastrar';
            $dados['urlaction'] = SITE_URL . "/Entregas/Inserir";
            $dados['titulopagina'] = "Novo Meio de Entrega";
        }
        parent::prepararView("Config/cad_meio_entrega", $dados);
    }

    function Inserir()
    {
        self::validarCamposObrigatorio();

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $codigo_entrega = null;//filter_input(INPUT_POST, "codigo_entrega");
        $empresa_responsavel = filter_input(INPUT_POST, "empresa_responsavel");
        $valor_cobrado = filter_input(INPUT_POST, "valor_cobrado");
        $prazo_maximo = filter_input(INPUT_POST, "prazo_maximo");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");
        $restricao = filter_input(INPUT_POST, "restricao");
        $cal_frete = filter_input(INPUT_POST, "cal_frete");

        $cidades = filter_input(INPUT_POST, "cidade", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $estados = filter_input(INPUT_POST, "estado", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        $pasta = 'meiosentregas';

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 250, 'altura' => 200);

                $ret = parent::salvarImagem($_FILES['foto'], $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'MEIO_ID' => 0,
                'NOME_MEIO' => $nome,
                'DESCRICAO_MEIO' => $descricao,
                'CODIGO_MEIO' => empty($codigo_entrega) ? 0 : $codigo_entrega,
                'EMPRESA' => $empresa_responsavel,
                'RESTRICAO' => $restricao,
                'VALOR_COBRADO' => empty($valor_cobrado) ? 0 : Common::returnValor($valor_cobrado),
                'PRAZO_MAXIMO' => $prazo_maximo,
                'ME_STATUS' => $status,
                'CALC_FRETE' => $cal_frete,
                'IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : null,
                'CIDADES' => implode(',', $cidades),
                'ESTADOS' => implode(',', $estados)
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarMeioEntrega($dados));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Entregas');
            } else {
                $msg = 'Entrega cadastrado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Entregas');
            }
        }
    }

    function Editar()
    {
        $id = filter_input(INPUT_POST, "id");

        $submit = filter_input(INPUT_POST, "submit");
        $nome = filter_input(INPUT_POST, "nome");
        $codigo_entrega = null;//filter_input(INPUT_POST, "codigo_entrega");
        $empresa_responsavel = filter_input(INPUT_POST, "empresa_responsavel");
        $valor_cobrado = filter_input(INPUT_POST, "valor_cobrado");
        $prazo_maximo = filter_input(INPUT_POST, "prazo_maximo");
        $status = filter_input(INPUT_POST, "status");
        $descricao = filter_input(INPUT_POST, "descricao");
        $restricao = filter_input(INPUT_POST, "restricao");
        $cal_frete = filter_input(INPUT_POST, "cal_frete");

        $cidades = filter_input(INPUT_POST, "cidade", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);
        $estados = filter_input(INPUT_POST, "estado", FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

        $token = Session::get('token');
        $meio = Common::retornoWSLista($this->model->DadosMeioEntrega($token, $id));

        self::validarCamposObrigatorio($id, $meio['IMAGEM']);

        $pasta = 'meiosentregas';

        if (isset($submit)) {
            $imagens = [];

            if (!empty($_FILES['foto']['name'])) {

                $config = array('tamanho' => 4999999, 'largura' => 250, 'altura' => 200);

                $ret = parent::salvarImagem($_FILES['foto'], $pasta, $config);
                array_push($imagens, $pasta . "/" . $ret['imagem']);
            }

            $dados = [
                'TOKEN' => Session::get('token'),
                'MEIO_ID' => $id,
                'NOME_MEIO' => $nome,
                'DESCRICAO_MEIO' => $descricao,
                'CODIGO_MEIO' => empty($codigo_entrega) ? 0 : $codigo_entrega,
                'EMPRESA' => $empresa_responsavel,
                'RESTRICAO' => $restricao,
                'VALOR_COBRADO' => empty($valor_cobrado) ? 0 : Common::returnValor($valor_cobrado),
                'PRAZO_MAXIMO' => intval($prazo_maximo),
                'ME_STATUS' => $status,
                'CALC_FRETE' => $cal_frete,
                'IMAGEM' => count($imagens) > 0 ? implode(',', $imagens) : $meio['IMAGEM'],
                'CIDADES' => !empty($cidades) ? implode(',', $cidades) : '',
                'ESTADOS' => !empty($estados) ? implode(',', $estados) : ''
            ];

            $cadastrar = Common::retornoWSLista($this->model->CadastrarEditarMeioEntrega($dados));

            if ($cadastrar['O_COD_RETORNO'] != 0) {
                $msg = $cadastrar['O_DESC_CURTO'];
                $situacao = 'danger';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Entregas');
            } else {
                $msg = 'Entrega editado com sucesso!';
                $situacao = 'success';

                Common::alert($msg, $situacao, 'acao');
                Common::redir('Entregas');
            }
        }
    }

    function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");
        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'MEIO_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirMeioEntrega($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Entregas';
        } else {
            $msg = 'Entrega deletado com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Entregas';
        }
    }

    private function validarCamposObrigatorio($id = null, $img = null)
    {
        $dados['Título'] = filter_input(INPUT_POST, "nome");
        $dados['Descrição'] = filter_input(INPUT_POST, "descricao");

        if (empty($id)) {
            $dados['Foto'] = $_FILES['foto']['name'];
        } else {
            if (empty($img)) {
                $dados['Foto'] = $_FILES['foto']['name'];
            }
        }

        if (!empty($id)) {
            Common::validarInputsObrigatorio($dados, 'Entregas/MeioEntrega/' . $id);
        } else {
            Common::validarInputsObrigatorio($dados, 'Entregas/MeioEntrega');
        }
    }
}
