<?php

namespace Application\Controller\Vendas;

use avalcelular\Controller,
    avalcelular\Common,
    avalcelular\Session;

class Propostas extends Controller
{

    function __construct()
    {
        parent::__construct();
        parent::loadModel('Application\Model\ModelRelatorios', 'model');

        if (empty(Session::get('token'))) {
            Session::destroy();
            Common::redir('Login');
        }
    }

    public function main()
    {
        $token = Session::get('token');

        $dados['titulopagina'] = "Propostas Realizadas";
        $dados['listapropostas'] = Common::retornoWSLista($this->model->ListaPropostasRealizadas($token), 1);
        parent::prepararView("Vendas/pag_propostas",  $dados);
    }

    public function Detalhe($id)
    {
        $token = Session::get('token');
        $dados['proposta'] = Common::retornoWSLista($this->model->DetalheVenda($token, $id));

        Common::exibirDadosTeste($dados);
        parent::prepararBasicView("Vendas/pag_modal_detalhe", $dados);
    }

    public function Excluir()
    {
        $id = filter_input(INPUT_POST, "id");

        $token = Session::get('token');

        $dados = [
            'TOKEN' => $token,
            'VEN_ID' => $id
        ];

        $deletar = Common::retornoWSLista($this->model->ExcluirVenda($dados));

        if ($deletar['O_COD_RETORNO'] != 0) {
            $msg = 'Erro ao excluir a venda, tente novamente mais tarde! ' . $deletar['O_DESC_CURTO'];
            $situacao = 'danger';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Propostas';
        } else {
            $msg = 'Venda excluída com sucesso!';
            $situacao = 'success';

            Common::alert($msg, $situacao, 'acao');
            return SITE_URL . '/Propostas';
        }
    }

    public function Finalizar($id)
    {
        $token = Session::get('token');

        $parametros = [
            'I_TOKEN' => $token,
            'VEN_ID' => $id,
            'VEN_STATUS' => 1
        ];

        $status = Common::retornoWSLista($this->model->FinalizarVenda($parametros));

        if ($status['O_COD_RETORNO'] != 0) {
            $msg = $status['O_DESC_CURTO'];
            $situacao = 'danger';

            echo json_encode(['O_COD_RETORNO' => $status['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        } else {
            $msg = 'Finalizado com sucesso!';
            $situacao = 'success';

            echo json_encode(['O_COD_RETORNO' => $status['O_COD_RETORNO'], 'O_DESC_CURTO' => $msg]);
        }
    }
}
