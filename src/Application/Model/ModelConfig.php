<?php

namespace Application\Model;

use avalcelular\Model,
    avalcelular\Common;

class ModelConfig extends Model
{

    public function DadosMenu($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MN_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_MENU(:I_TOKEN, :I_MN_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarMenus($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MN_ID' => $dados['MN_ID'],
            'I_MN_NOME' => $dados['MN_NOME'],
            'I_MN_DESC' => $dados['MN_DESC'],
            'I_MN_LINK' => $dados['MN_LINK'],
            'I_MN_PAI' => $dados['MN_PAI'],
            'I_MN_IMG' => $dados['MN_IMG']
        ];

        $sql = "CALL PRC_IN_ED_MENU(:I_TOKEN, :I_MN_ID, :I_MN_NOME, :I_MN_DESC, :I_MN_LINK, :I_MN_PAI, :I_MN_IMG)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirMenu($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MN_ID' => $dados['MN_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_MENU(:I_TOKEN, :I_MN_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ConfiguracoesADM($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_DADOS_CONFIG(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function SalvarSisInfoContato($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_ENDERECO' => $dados['ENDERECO'],
            'I_BAIRRO' => $dados['BAIRRO'],
            'I_NUMERO' => $dados['NUMERO'],
            'I_CIDADE' => $dados['CIDADE'],
            'I_UF' => $dados['UF'],
            'I_CEP' => $dados['CEP'],
            'I_COMPLEMENTO' => $dados['COMPLEMENTO'],
            'I_TELEFONE' => $dados['TELEFONE'],
            'I_EMAIL_TECNICO' => $dados['EMAIL_TECNICO']
        ];

        $sql = "CALL PRC_SALVAR_INFO_CONTATO(:I_TOKEN, :I_ID, :I_ENDERECO, :I_BAIRRO, :I_NUMERO, :I_CIDADE, :I_UF, :I_CEP, :I_COMPLEMENTO, :I_TELEFONE, :I_EMAIL_TECNICO)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function SalvarSisInfoSMTP($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_SMTP_HOST' => Common::encrypt_decrypt('encrypt', $dados['SMTP_HOST']),
            'I_SMTP_PORTA' => Common::encrypt_decrypt('encrypt', $dados['SMTP_PORTA']),
            'I_SMTP_USER' => Common::encrypt_decrypt('encrypt', $dados['SMTP_USER']),
            'I_SMTP_SENHA' => Common::encrypt_decrypt('encrypt', $dados['SMTP_SENHA']),
            'I_MAIL_CONTATO_DEFAULT' => Common::encrypt_decrypt('encrypt', $dados['MAIL_CONTATO_DEFAULT']),
            'I_NOME_CONTATO_DEFAULT' => $dados['NOME_CONTATO_DEFAULT']
        ];

        $sql = "CALL PRC_SALVAR_INFO_SMTP(:I_TOKEN, :I_ID, :I_SMTP_HOST, :I_SMTP_PORTA, :I_SMTP_USER, :I_SMTP_SENHA, :I_MAIL_CONTATO_DEFAULT, :I_NOME_CONTATO_DEFAULT)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function SalvarSisImagens($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ID' => $dados['ID'],
            'I_LOGOTIPO' => $dados['LOGOTIPO'],
            'I_FAVICON' => $dados['FAVICON']
        ];

        $sql = "CALL PRC_SALVAR_INFO_IMAGENS(:I_TOKEN, :I_ID, :I_LOGOTIPO, :I_FAVICON)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaMeiosEntregaCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_MEIOS_ENTREGA(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosMeioEntrega($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MEIO_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_MEIO_ENTREGA(:I_TOKEN, :I_MEIO_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaCidadesAbrangencia($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MEIO_ID' => $id
        ];

        $sql = "CALL PRC_LISTAR_CIDADES_ABRANGENCIA(:I_TOKEN, :I_MEIO_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarMeioEntrega($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MEIO_ID' => $dados['MEIO_ID'],
            'I_NOME_MEIO' => $dados['NOME_MEIO'],
            'I_DESCRICAO_MEIO' => $dados['DESCRICAO_MEIO'],
            'I_CODIGO_MEIO' => $dados['CODIGO_MEIO'],
            'I_EMPRESA' => $dados['EMPRESA'],
            'I_RESTRICAO' => $dados['RESTRICAO'],
            'I_VALOR_COBRADO' => $dados['VALOR_COBRADO'],
            'I_PRAZO_MAXIMO' => $dados['PRAZO_MAXIMO'],
            'I_ME_STATUS' => $dados['ME_STATUS'],
            'I_CALC_FRETE' => $dados['CALC_FRETE'],
            'I_IMAGEM' => $dados['IMAGEM'],
            'I_ESTADOS' => $dados['ESTADOS'],
            'I_CIDADES' => $dados['CIDADES']
        ];

        $sql = "CALL PRC_IN_ED_MEIO_ENTREGA(:I_TOKEN, :I_MEIO_ID, :I_NOME_MEIO, :I_DESCRICAO_MEIO, :I_CODIGO_MEIO, :I_EMPRESA, :I_RESTRICAO, :I_VALOR_COBRADO, :I_PRAZO_MAXIMO, :I_ME_STATUS, :I_CALC_FRETE, :I_IMAGEM, :I_ESTADOS, :I_CIDADES);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirMeioEntrega($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MEIO_ID' => $dados['MEIO_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_MEIO_ENTREGA(:I_TOKEN, :I_MEIO_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaColetasCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_COLETAS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosColeta($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CLT_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_COLETAS(:I_TOKEN, :I_CLT_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarColetas($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
			'I_CLT_ID' => $dados['CLT_ID'],
            'I_CLT_DESC' => $dados['CLT_DESC'],
			'I_CLT_VAL_DESC' => $dados['CLT_VAL_DESC'],
			'I_MEIO_ID' => $dados['MEIO_ID'],
			'I_CLT_STATUS' => $dados['CLT_STATUS']
		];

        $sql = "CALL PRC_IN_ED_COLETAS(:I_TOKEN, :I_CLT_ID, :I_CLT_DESC, :I_CLT_VAL_DESC, :I_MEIO_ID, :I_CLT_STATUS);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }


    public function ExcluirColeta($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CLT_ID' => $dados['CLT_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_COLETAS(:I_TOKEN, :I_CLT_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }
}
