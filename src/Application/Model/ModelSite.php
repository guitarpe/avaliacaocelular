<?php

namespace Application\Model;

use avalcelular\Model,
    avalcelular\Common;

class ModelSite extends Model
{
    public function ListaTiposCadastrados()
    {
        $parametros = [
            'I_OPCAO' => 1,
            'I_TIPO' => 0
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaModelosCadastrados($tipo)
    {
        $parametros = [
            'I_OPCAO' => 2,
            'I_TIPO' => $tipo
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaRedesCadastradas($tipo)
    {
        $parametros = [
            'I_OPCAO' => 3,
            'I_TIPO' => $tipo
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaCapacidadesCadastrados($tipo)
    {
        $parametros = [
            'I_OPCAO' => 4,
            'I_TIPO' => $tipo
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaAcessoriosCadastrados($tipo)
    {
        $parametros = [
            'I_OPCAO' => 5,
            'I_TIPO' => $tipo
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaAvariasCadastrados($tipo)
    {
        $parametros = [
            'I_OPCAO' => 6,
            'I_TIPO' => $tipo
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaColetasCadastrados()
    {
        $parametros = [
            'I_OPCAO' => 7,
            'I_TIPO' => 0
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaLojasCadastrados()
    {
        $parametros = [
            'I_OPCAO' => 8,
            'I_TIPO' => 0
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaCidadesAbrangenciaCadastrados($meio)
    {
        $parametros = [
            'I_OPCAO' => 9,
            'I_TIPO' => $meio
        ];

        $sql = "CALL PRC_LISTAR_DADOS_VENDA(:I_OPCAO, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function TipoPorApelido($apelido){
        $parametros = [
            'APELIDO' => $apelido
        ];

        $sql = "SELECT
                    TP_ID, TP_DESC
                FROM TIPOS_APARELHOS
                WHERE TP_APELIDO=:APELIDO";

        $resultado = parent::selectData($sql,$parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ModeloPorApelido($apelido){
        $parametros = [
            'APELIDO' => $apelido
        ];

        $sql = "SELECT
                    MOD_ID, MOD_DESC
                FROM MODELOS_APARELHOS
                WHERE MOD_APELIDO=:APELIDO";

        $resultado = parent::selectData($sql,$parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CapacidadePorApelido($modelo, $apelido){
        $parametros = [
            'APELIDO' => $apelido,
            'MOD_ID' => $modelo
        ];

        $sql = "SELECT
                    CAP_ID, CAP_DESC, CAP_VAL_AD
                FROM CAPACIDADES_APARELHOS
                WHERE MOD_ID=:MOD_ID
                    AND CAP_APELIDO=:APELIDO";

        $resultado = parent::selectData($sql,$parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosAparelho($modelo, $dados)
    {
        $parametros = [
            'I_MOD_ID' => $modelo,
            'I_REDE' => '',
            'I_CAPACIDADE' => $dados['capacidade'],
            'I_ACESSORIOS' => '',
            'I_AVARIAS' => $dados['avarias']
        ];

        $sql = "CALL PRC_DADOS_APARELHO_VENDA(:I_MOD_ID, :I_REDE, :I_CAPACIDADE, :I_ACESSORIOS, :I_AVARIAS);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaAvarias($modelo, $avarias){
        $parametros = [
            'MOD_ID' => $modelo
        ];

        $sql = "SELECT
                    AVA_DESC, AVA_VAL_DESC
                FROM AVARIAS_APARELHOS
                WHERE MOD_ID=:MOD_ID
                    AND AVA_ID IN(".$avarias.")";

        $resultado = parent::selectData($sql,$parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaMeiosEntregasCadastrados(){
        $parametros = [];

        $sql = "SELECT
                    MEIO_ID, NOME_MEIO, IMAGEM, VALOR_COBRADO
                FROM MEIOS_ENTREGA
                WHERE ME_STATUS=1";
        $resultado = parent::selectData($sql,$parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function RegistrarProposta($dados){

        $parametros = [
            'I_SESSAO' => $dados['SESSAO'],
            'I_IP' => $dados['IP'],
            'I_TIPO' => $dados['TIPO'],
            'I_MODELO' => $dados['MODELO'],
            'I_REDE' => $dados['REDE'],
            'I_CAPACIDADE' => $dados['CAPACIDADE'],
            'I_ACESSORIOS' => $dados['ACESSORIOS'],
            'I_AVARIAS' => $dados['AVARIAS'],
            'I_COLETA' => $dados['COLETA'],
            'I_VAL_PROPOSTA' => $dados['VAL_PROPOSTA'],
            'I_OBSERVACAO' => $dados['OBSERVACAO']
        ];

        $sql = "CALL PRC_CADASTRO_PROPOSTA(:I_SESSAO, :I_IP, :I_TIPO, :I_MODELO, :I_REDE, :I_CAPACIDADE, :I_ACESSORIOS, :I_AVARIAS, :I_COLETA, :I_VAL_PROPOSTA, :I_OBSERVACAO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosProposta($id){
        $parametros = [
            'PROP_ID' => $id
        ];

        $sql = "SELECT
                    T1.PROP_ID,
                    T1.PROP_STATUS,
                    T1.PROP_TIPO,
                    T1.PROP_MODELO,
                    T1.PROP_CAPACIDADE,
                    T1.PROP_COLETA,
                    T1.PROP_SESSAO,
                    T1.PROP_IP,
                    T1.PROP_DT_CAD,
                    T1.PROP_VAL_PROPOSTA,
                    T2.CLT_ID,
                    T2.CLT_DESC,
                    T2.CLT_VAL_DESC,
                    T2.CLT_STATUS,
                    T3.MEIO_ID,
                    T3.NOME_MEIO,
                    (CASE WHEN T3.NOME_MEIO LIKE 'MOTOBOY%' THEN 2 ELSE 1 END) OPCAO,
                    T3.DESCRICAO_MEIO,
                    T3.IMAGEM,
                    T3.CODIGO_MEIO,
                    T3.EMPRESA,
                    T3.RESTRICAO_ABRANG,
                    T3.VALOR_COBRADO,
                    T3.ME_STATUS,
                    T3.CALC_FRETE,
                    T3.PRAZO_MAXIMO
                FROM PROPOSTAS T1
                    INNER JOIN COLETAS_ENTREGA T2 ON T2.CLT_ID=T1.PROP_COLETA
                    INNER JOIN MEIOS_ENTREGA T3 ON T2.MEIO_ID=T3.MEIO_ID
                WHERE T1.PROP_ID=:PROP_ID";

        $resultado = parent::selectData($sql,$parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function FinalizarProposta($dados){

        $parametros = [
            'I_PROP_ID' => $dados['PROP_ID'],
            'I_VEN_NOME_COMPLETO' => $dados['VEN_NOME_COMPLETO'],
            'I_VEN_EMAIL' => $dados['VEN_EMAIL'],
            'I_VEN_DOC' => $dados['VEN_DOC'],
            'I_VEN_TELEFONE' => $dados['VEN_TELEFONE'],
            'I_VEN_OP_ENTREGA' => $dados['VEN_OP_ENTREGA'],
            'I_INFO_ID' => $dados['INFO_ID'],
            'I_VEN_ENDERECO' => $dados['VEN_ENDERECO'],
            'I_VEN_NUMERO' => $dados['VEN_NUMERO'],
            'I_VEN_COMPLEMENTO' => $dados['VEN_COMPLEMENTO'],
            'I_VEN_BAIRRO' => $dados['VEN_BAIRRO'],
            'I_VEN_CEP' => $dados['VEN_CEP'],
            'I_VEN_UF' => $dados['VEN_UF']
        ];

        Common::exibirDadosTeste($dados);
        $sql = "CALL PRC_CADASTRO_CHECKOUT_VENDA(:I_PROP_ID, :I_VEN_NOME_COMPLETO, :I_VEN_EMAIL, :I_VEN_DOC, :I_VEN_TELEFONE, :I_VEN_OP_ENTREGA, :I_INFO_ID, :I_VEN_ENDERECO, :I_VEN_NUMERO, :I_VEN_COMPLEMENTO, :I_VEN_BAIRRO, :I_VEN_CEP, :I_VEN_UF);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }
}
