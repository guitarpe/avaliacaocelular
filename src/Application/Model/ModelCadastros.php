<?php

namespace Application\Model;

use avalcelular\Model,
    avalcelular\Common;

class ModelCadastros extends Model
{

    public function ListaTiposCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_TIPOS_APARELHOS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosTipo($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TP_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_TIPOS_APARELHOS(:I_TOKEN, :I_TP_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarTipos($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_TP_ID' => $dados['TP_ID'],
            'I_TP_DESC' => $dados['TP_DESC'],
            'I_TP_IMAGEM' => $dados['TP_IMAGEM'],
            'I_TP_APELIDO' => $dados['TP_APELIDO']
        ];

        $sql = "CALL PRC_IN_ED_TIPOS_APARELHOS(:I_TOKEN, :I_TP_ID, :I_TP_DESC, :I_TP_IMAGEM, :I_TP_APELIDO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirTipo($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_TP_ID' => $dados['TP_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_TIPOS_APARELHOS(:I_TOKEN, :I_TP_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaModelosCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_MODELOS_APARELHOS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaModelosPorTipo($tipo)
    {
        $parametros = [
            'TP_ID' => $tipo
        ];

        $sql = "SELECT 
                    MOD_ID, MOD_DESC 
                FROM MODELOS_APARELHOS 
                WHERE TP_ID=:TP_ID 
                ORDER BY MOD_DESC ASC";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosModelo($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_MOD_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_MODELOS_APARELHOS(:I_TOKEN, :I_MOD_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarModelos($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
			'I_TP_ID' => $dados['TP_ID'],
            'I_MOD_ID' => $dados['MOD_ID'],
            'I_MOD_PRC_VENDA' => $dados['MOD_PRC_VENDA'],
            'I_MOD_PRC_COMPRA' => $dados['MOD_PRC_COMPRA'],
            'I_MOD_DESC' => $dados['MOD_DESC'],
            'I_MOD_APELIDO' => $dados['MOD_APELIDO']
        ];

        $sql = "CALL PRC_IN_ED_MODELOS_APARELHOS(:I_TOKEN, :I_TP_ID, :I_MOD_ID, :I_MOD_PRC_VENDA, :I_MOD_PRC_COMPRA, :I_MOD_DESC, :I_MOD_APELIDO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirModelo($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_MOD_ID' => $dados['MOD_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_MODELOS_APARELHOS(:I_TOKEN, :I_MOD_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaRedesCadastradas($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_REDES_APARELHOS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosRede($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_RED_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_REDES_APARELHOS(:I_TOKEN, :I_RED_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarRedes($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
			'I_MOD_ID' => $dados['MOD_ID'],
            'I_RED_ID' => $dados['RED_ID'],
            'I_RED_DESC' => $dados['RED_DESC'],
            'I_RED_APELIDO' => $dados['RED_APELIDO'],
            'I_RED_VAL_AD' => $dados['RED_VAL_AD']
        ];

        $sql = "CALL PRC_IN_ED_REDES_APARELHOS(:I_TOKEN, :I_MOD_ID, :I_RED_ID, :I_RED_DESC, :I_RED_APELIDO, :I_RED_VAL_AD);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirRede($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_RED_ID' => $dados['RED_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_REDES_APARELHOS(:I_TOKEN, :I_RED_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }
    
    public function ListaAcessoriosCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_ACESSORIOS_APARELHOS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosAcessorio($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_ACS_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_ACESSORIOS_APARELHOS(:I_TOKEN, :I_ACS_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarAcessorios($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
			'I_MOD_ID' => $dados['MOD_ID'],
            'I_ACS_ID' => $dados['ACS_ID'],
            'I_ACS_DESC' => $dados['ACS_DESC'],
            'I_ACS_VAL_AD' => $dados['ACS_VAL_AD']
        ];

        $sql = "CALL PRC_IN_ED_ACESSORIOS_APARELHOS(:I_TOKEN, :I_MOD_ID, :I_ACS_ID, :I_ACS_DESC, :I_ACS_VAL_AD);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirAcessorio($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_ACS_ID' => $dados['ACS_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_ACESSORIOS_APARELHOS(:I_TOKEN, :I_ACS_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaCapacidadesCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_CAPACIDADES_APARELHOS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosCapacidade($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_CAP_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_CAPACIDADES_APARELHOS(:I_TOKEN, :I_CAP_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarCapacidades($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
			'I_MOD_ID' => $dados['MOD_ID'],
            'I_CAP_ID' => $dados['CAP_ID'],
            'I_CAP_DESC' => $dados['CAP_DESC'],
            'I_CAP_VAL_AD' => $dados['CAP_VAL_AD'],
            'I_CAP_APELIDO' => $dados['CAP_APELIDO']
        ];

        $sql = "CALL PRC_IN_ED_CAPACIDADES_APARELHOS(:I_TOKEN, :I_MOD_ID, :I_CAP_ID, :I_CAP_DESC, :I_CAP_VAL_AD, :I_CAP_APELIDO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirCapacidade($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_CAP_ID' => $dados['CAP_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_CAPACIDADES_APARELHOS(:I_TOKEN, :I_CAP_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaAvariasCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_AVARIAS_APARELHOS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosAvaria($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_AVA_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_AVARIAS_APARELHOS(:I_TOKEN, :I_AVA_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarAvarias($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
			'I_MOD_ID' => $dados['MOD_ID'],
            'I_AVA_ID' => $dados['AVA_ID'],
            'I_AVA_DESC' => $dados['AVA_DESC'],
            'I_AVA_VAL_DESC' => $dados['AVA_VAL_DESC'],
            'I_AVA_OP_OUTRO' => $dados['AVA_OP_OUTRO']
        ];

        $sql = "CALL PRC_IN_ED_AVARIAS_APARELHOS(:I_TOKEN, :I_MOD_ID, :I_AVA_ID, :I_AVA_DESC, :I_AVA_VAL_DESC, :I_AVA_OP_OUTRO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirAvaria($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_AVA_ID' => $dados['AVA_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_AVARIAS_APARELHOS(:I_TOKEN, :I_AVA_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaLojasCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_LOJAS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosLoja($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_LOJ_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_LOJAS(:I_TOKEN, :I_LOJ_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarLojas($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
			'I_LOJ_ID' => $dados['LOJ_ID'],
            'I_LOJ_RAZAO_SOCIAL' => $dados['LOJ_RAZAO_SOCIAL'],
			'I_LOJ_ENDERECO_COMPL' => $dados['LOJ_ENDERECO_COMPL'],
			'I_LOJ_COMPLEMENTO' => $dados['LOJ_COMPLEMENTO'],
            'I_LOJ_CEP' => $dados['LOJ_CEP'],
            'I_LOJ_TELEFONE' => $dados['LOJ_TELEFONE'],
			'I_LOJ_ATENDIMENTO_SEM' => $dados['LOJ_ATENDIMENTO_SEM'],
			'I_LOJ_ATENDIMENTO_FS' => $dados['LOJ_ATENDIMENTO_FS'],
			'I_LOJ_ATENDIMENTO_FERIADO' => $dados['LOJ_ATENDIMENTO_FERIADO']
		];

        $sql = "CALL PRC_IN_ED_LOJAS(:I_TOKEN, :I_LOJ_ID, :I_LOJ_RAZAO_SOCIAL, :I_LOJ_ENDERECO_COMPL, :I_LOJ_COMPLEMENTO, :I_LOJ_CEP, :I_LOJ_TELEFONE, :I_LOJ_ATENDIMENTO_SEM, :I_LOJ_ATENDIMENTO_FS, :I_LOJ_ATENDIMENTO_FERIADO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirLoja($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_LOJ_ID' => $dados['LOJ_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_LOJAS(:I_TOKEN, :I_LOJ_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }
}
