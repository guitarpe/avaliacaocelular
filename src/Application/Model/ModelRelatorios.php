<?php

namespace Application\Model;

use avalcelular\Model;

class ModelRelatorios extends Model
{

    public function ListaRelatorioAcessosAdmin($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TIPO' => 1
        ];

        $sql = "CALL PRC_RELATORIOS(:I_TOKEN, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaRelatorioAcessosSite($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TIPO' => 2
        ];

        $sql = "CALL PRC_RELATORIOS(:I_TOKEN, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaPropostasRealizadas($token)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_TIPO' => 3
        ];

        $sql = "CALL PRC_RELATORIOS(:I_TOKEN, :I_TIPO);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DetalheVenda($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_VEN_ID' => $id
        ];

        $sql = "CALL PRC_DETALHE_VENDA(:I_TOKEN, :I_VEN_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirVenda($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_VEN_ID' => $dados['VEN_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_VENDA(:I_TOKEN, :I_VEN_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function FinalizarVenda($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_VEN_ID' => $dados['VEN_ID'],
            'I_VEN_STATUS' => $dados['VEN_STATUS']
        ];

        $sql = "CALL PRC_FINALIZAR_VENDA(:I_TOKEN, :I_VEN_ID, :I_VEN_STATUS);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }
}
