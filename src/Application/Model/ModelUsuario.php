<?php

namespace Application\Model;

use avalcelular\Common;
use avalcelular\Model;

class ModelUsuario extends Model
{

    public function ListaPerfisCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_PERFIS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosPerfil($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_PER_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_PERFIS(:I_TOKEN, :I_PER_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarPerfis($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PER_ID' => $dados['PER_ID'],
            'I_PER_SIGLA' => $dados['PER_SIGLA'],
            'I_PER_DESC' => $dados['PER_DESC'],
            'I_PER_PERMISSIONS' => $dados['PER_PERMISSIONS']
        ];

        $sql = "CALL PRC_IN_ED_PERFIL(:I_TOKEN, :I_PER_ID, :I_PER_SIGLA, :I_PER_DESC, :I_PER_PERMISSIONS);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirPerfil($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_PER_ID' => $dados['PER_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_PERFIL(:I_TOKEN, :I_PER_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ListaUsuariosCadastrados($token)
    {
        $parametros = [
            'I_TOKEN' => $token
        ];

        $sql = "CALL PRC_LISTAR_USUARIOS(:I_TOKEN);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function DadosUsuario($token, $id)
    {
        $parametros = [
            'I_TOKEN' => $token,
            'I_US_ID' => $id
        ];

        $sql = "CALL PRC_DADOS_USUARIO(:I_TOKEN, :I_US_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function AtivarInativarUsuario($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['I_TOKEN'],
            'I_US_ID' => $dados['US_ID'],
            'I_US_STATUS' => $dados['US_STATUS']
        ];

        $sql = "CALL PRC_ATIVAR_INATIVAR_USUARIO(:I_TOKEN, :I_US_ID, :I_US_STATUS);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function CadastrarEditarUsuarios($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_US_ID' => $dados['US_ID'],
            'I_PER_ID' => $dados['PER_ID'],
            'I_US_LOGIN' => $dados['US_LOGIN'],
            'I_US_PASS' => $dados['US_PASS'],
            'I_US_STATUS' => $dados['US_STATUS'],
            'I_US_IMAGE' => $dados['US_IMAGEM'],
            'I_NOME_USER' => $dados['NOME_USER'],
            'I_EMAIL_USER' => $dados['EMAIL_USER'],
            'I_GENERO_USER' => $dados['GENERO_USER'],
            'I_TEL_USER' => $dados['TEL_USER'],
            'I_PASTA_USER' => $dados['PASTA_USER']
        ];

        $sql = "CALL PRC_IN_ED_USUARIO(:I_TOKEN, :I_US_ID, :I_PER_ID, :I_US_LOGIN, :I_US_PASS, :I_US_STATUS, :I_US_IMAGE, :I_NOME_USER, :I_EMAIL_USER, :I_GENERO_USER, :I_PASTA_USER, :I_TEL_USER);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function ExcluirUsuario($dados)
    {
        $parametros = [
            'I_TOKEN' => $dados['TOKEN'],
            'I_US_ID' => $dados['US_ID']
        ];

        $sql = "CALL PRC_EXCLUIR_USUARIO(:I_TOKEN, :I_US_ID);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }
    #METODOS DA TELA DE LOGIN

    public function ValidaLogin($login, $senha)
    {
        $pass = strtoupper(md5($login . $senha));

        $parametros = [
            'I_US_LOGIN' => $login,
            'I_US_SENHA' => $pass
        ];

        $sql = "CALL PRC_LOGAR_SISTEMA(:I_US_LOGIN, :I_US_SENHA)";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function TrocarSenhaReset($dados)
    {
        $parametros = [
            'I_US_ID' => $dados['US_ID'],
            'I_US_EMAIL' => $dados['US_LOGIN'],
            'I_US_SENHA' => $dados['US_SENHA'],
            'I_US_STATUS' => $dados['US_STATUS']
        ];

        $sql = "CALL PRC_ALTERAR_SENHA(:I_US_ID, :I_US_EMAIL, :I_US_SENHA, :I_US_STATUS);";
        $resultado = parent::callprocedure($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }
    }

    public function UsuarioPorID($id)
    {
        $parametros = [
            'I_US_ID' => $id,
        ];

        $sql = "SELECT
                    US.US_ID, US.PER_ID, US.US_LOGIN, US.US_STATUS, US.US_IMAGEM,
                    DU.NOME_COMPLETO, DU.EMAIL, DU.GENERO, DU.TELEFONE,
                    0 as O_COD_RETORNO, '' as O_DESC_CURTO, 'N' as O_TOKEN_INVALIDO
                FROM USUARIOS US
                    INNER JOIN DADOS_USUARIOS DU ON DU.US_ID=US.US_ID
                WHERE US.US_ID=:I_US_ID;";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }

    public function UsuarioPorEmail($email)
    {
        $parametros = [
            'I_MAIL_ID' => $email,
        ];

        $sql = "SELECT
                    US.US_ID, US.PER_ID, US.US_LOGIN, US.US_STATUS, US.US_IMAGEM,
                    DU.NOME_COMPLETO, DU.EMAIL, DU.GENERO, DU.TELEFONE,
                    0 as O_COD_RETORNO, '' as O_DESC_CURTO, 'N' as O_TOKEN_INVALIDO
                FROM USUARIOS US
                    INNER JOIN DADOS_USUARIOS DU ON DU.US_ID=US.US_ID
                WHERE DU.EMAIL=:I_MAIL_ID;";

        $resultado = parent::selectData($sql, $parametros);

        if ($resultado['erro'] == 1) {
            return 0;
        }

        return $resultado;
    }
}
