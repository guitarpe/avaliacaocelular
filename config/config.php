<?php
$url = filter_input(INPUT_SERVER, 'HTTP_HOST');

if ($url === 'localhost') {
    $diretorio = '/portfolio/avaliacaocelular';
    define('URL', 'http://localhost');
    define('SITE_URL', '/portfolio/avaliacaocelular');
    define('IMAGENS_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'img');
    define('FOTOUSUARIO_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'img/fotosusuarios');
    define('ENVIA_EMAIL', true);
} else {
    $diretorio = '/avaliacaocelular';
    define('URL', 'http://www.ihelpu.com.br');
    define('SITE_URL', $diretorio);
    define('IMAGENS_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'img');
    define('FOTOUSUARIO_PATH', substr(realpath(dirname(__FILE__)), 0, -7) . DIRECTORY_SEPARATOR . 'public_html' . DIRECTORY_SEPARATOR . 'static' . DIRECTORY_SEPARATOR . 'img/fotosusuarios');
    define('ENVIA_EMAIL', true);
}

define('TITLE_SITE', 'iHelpU – Avaliação - ADM');
define('URL_SITE', $diretorio);
define('IMAGEM_URL', $diretorio . '/public_html/static/img');
define('LOGS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/logs');
define('FOTOUSUARIOS_URL', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static/img/fotosusuarios');
define('PATHSTATIC', substr(realpath(dirname(__FILE__)), 0, -6) . 'public_html/static');
define('SYSTEM_PATH', substr(realpath(dirname(__FILE__)), 0, -6));
define('CONTROLLER_PATH', substr(realpath(dirname(__FILE__)), 0, -6) . 'src' . DIRECTORY_SEPARATOR . 'Application' . DIRECTORY_SEPARATOR . 'Controller');
define('URL_ATUAL', filter_input(INPUT_SERVER, 'HTTP_HOST') . filter_input(INPUT_SERVER, 'REQUEST_URI'));
define('PATH_STATIC_DATA', $_SERVER['DOCUMENT_ROOT'] . $diretorio . '/public_html/static/data');

//PÁGINA DE VENDA DO SITE
define('NOME_SITE', 'iHelpU – Avaliação');
define('SITE_CSS', $diretorio . '/public_html/static/Site/css/');
define('SITE_JS', $diretorio . '/public_html/static/Site/js/');
define('SITE_IMG', $diretorio . '/public_html/static/Site/img/');
define('TEMA_TWTY', $diretorio . '/public_html/static/Site/twentytwenty/');

define('URL_IMG_MAIL', 'http://www.ihelpu.com.br/avaliacao/public_html/static/img');
define('JS_URL', $diretorio . '/public_html/static/JavaScript');
define('CSS_URL', $diretorio . '/public_html/static/Css');
define('CSS_BOOTSTRAP_URL', $diretorio . '/public_html/static/Css/bootstrap');
define('CSS_DATAPICKER_URL', $diretorio . '/public_html/static/Css/datePicker');
define('CSS_FONT_ENTYPO_URL', $diretorio . '/public_html/static/Css/font-icons/entypo/css');
define('CSS_FONT_FA_URL', $diretorio . '/public_html/static/Css/font-icons/font-awesome/css');
define('CSS_GLOBAL_URL', $diretorio . '/public_html/static/Css/global');
define('CSS_JVECTORMAP_URL', $diretorio . '/public_html/static/Css/jvectormap');
define('CSS_RICKCHAW_URL', $diretorio . '/public_html/static/Css/rickshaw');
define('CSS_JQUERY_UI_URL', $diretorio . '/public_html/static/Css/jquery-ui');
define('URL_CSS_UIKIT_PATH', $diretorio . '/public_html/static/Css/Uikit');
define('URL_CSS_DATATABLE', $diretorio . '/public_html/static/Css/dataTable');
define('URL_CSS_PLUGIN_UPLOAD_URL', $diretorio . '/public_html/static/Css/pluginUpload');
define('JS_PLUGIN_UPLOAD_URL', $diretorio . '/public_html/static/JavaScript/pluginUpload');
define('JS_BOOTSTRAP_URL', $diretorio . '/public_html/static/JavaScript/bootstrap');
define('JS_ACESSO_URL', $diretorio . '/public_html/static/JavaScript/acesso');
define('JS_JQUERY_URL', $diretorio . '/public_html/static/JavaScript/jquery');
define('JS_JQUERY_UI_URL', $diretorio . '/public_html/static/JavaScript/jquery-ui');
define('JS_GLOBAL_URL', $diretorio . '/public_html/static/JavaScript/global');
define('JS_TINYMCE_URL', $diretorio . '/public_html/static/JavaScript/tinymce');
define('JS_GSAP_URL', $diretorio . '/public_html/static/JavaScript/gsap');
define('JS_DATAPICKER_URL', $diretorio . '/public_html/static/JavaScript/dataPicker');
define('JS_DATATABLE_URL', $diretorio . '/public_html/static/JavaScript/dataTable');
define('JS_SELECT2_URL', $diretorio . '/public_html/static/JavaScript/select2');
define('JS_JVECTORMAP_URL', $diretorio . '/public_html/static/JavaScript/jvectormap');
define('JS_RICKCHAW_URL', $diretorio . '/public_html/static/JavaScript/rickshaw');
define('JS_FULLCALENDAR_URL', $diretorio . '/public_html/static/JavaScript/fullcalendar');
define('URL_JS_UIKIT_PATH', $diretorio . '/public_html/static/JavaScript/Uikit');
define('CSS_LIGHTBOX', $diretorio . '/public_html/static/Css/lightbox');
define('JS_LIGHTBOX', $diretorio . '/public_html/static/JavaScript/lightbox');
define('PLUGIN_WAITME', $diretorio . '/public_html/static/plugins/waitMe');
define('JS_FANCYBOX', $diretorio . '/public_html/static/JavaScript/fancybox');
define('JS_TELAS', $diretorio . '/public_html/static/JavaScript/telas');
define('URL_ICONES', $diretorio . '/public_html/static/Icones');
define('URL_ICONES_ICONIC', $diretorio . '/public_html/static/Icones/iconic');
define('URL_IMG', $diretorio . '/public_html/static/img');
define('PASTAMODELOS_URL', $diretorio . '/public_html/static/modelos');
define('PATHSTATIC_URL', $diretorio . '/public_html/static');

define('PASTAS_CONTROLLER', 'Common/Config/Cadastros/Relatorios/Vendas/Site');
