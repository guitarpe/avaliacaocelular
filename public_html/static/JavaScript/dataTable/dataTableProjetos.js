$(document).ready(function() {

    $.fn.dataTable.moment('DD/MM/YYYY');

    $('.tabelaIDV').dataTable({
        "bPaginate": true,
        "order": [0, "desc"],
        "ordering": true,
        "info": true,
        "language":{
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ at&#233; _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 at&#233; 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por p&#225;gina",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate":{
                "sNext": "Pr&#243;ximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "&#218;ltimo"
            }
        }
    });
});