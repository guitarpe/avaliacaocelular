/**
 *	Neon Calendar Script
 *
 *	Developed by Arlind Nushi - www.laborator.co
 */

var neonCalendar = neonCalendar || {};

;
(function ($, window, undefined)
{
    "use strict";

    $(document).ready(function ()
    {
        neonCalendar.$container = $(".calendar-env");

        $.extend(neonCalendar, {
            isPresent: neonCalendar.$container.length > 0
        });

        // Mail Container Height fit with the document
        if (neonCalendar.isPresent)
        {
            neonCalendar.$sidebar = neonCalendar.$container.find('.calendar-sidebar');
            neonCalendar.$body = neonCalendar.$container.find('.calendar-body');

            // Setup Calendar
            if ($.isFunction($.fn.fullCalendar))
            {

            } else
            {
                alert("Please include full-calendar script!");
            }
        }
    });

})(jQuery, window);


function fit_calendar_container_height()
{
    if (neonCalendar.isPresent)
    {
        if (neonCalendar.$sidebar.height() < neonCalendar.$body.height())
        {
            neonCalendar.$sidebar.height(neonCalendar.$body.height());
        } else
        {
            var old_height = neonCalendar.$sidebar.height();

            neonCalendar.$sidebar.height('');

            if (neonCalendar.$sidebar.height() < neonCalendar.$body.height())
            {
                neonCalendar.$sidebar.height(old_height);
            }
        }
    }
}

function reset_calendar_container_height()
{
    if (neonCalendar.isPresent)
    {
        neonCalendar.$sidebar.height('auto');
    }
}