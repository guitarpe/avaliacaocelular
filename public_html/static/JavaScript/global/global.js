jQuery(document).ready(function ($) {

    $('#modal-lg').on('show.bs.modal', function (e) {
        $(this).find('.modal-body').html("");

        var loadurl = $(e.relatedTarget).data('load-url');
        var title = $(e.relatedTarget).data('load-title');
        $(this).find('.modal-title').text(title);
        $(this).find('.modal-body').load(loadurl);
    });

    $('#modal-md').on('show.bs.modal', function (e) {
        $(this).find('.modal-body').html("");

        var loadurl = $(e.relatedTarget).data('load-url');
        var title = $(e.relatedTarget).data('load-title');
        $(this).find('.modal-title').text(title);
        $(this).find('.modal-body').load(loadurl);
    });

    $('#modal-sm').on('show.bs.modal', function (e) {
        $(this).find('.modal-body').html("");

        var loadurl = $(e.relatedTarget).data('load-url');
        var title = $(e.relatedTarget).data('load-title');
        $(this).find('.modal-title').text(title);
        $(this).find('.modal-body').load(loadurl);
    });

    $('[data-toggle="tooltip"]').tooltip();
    $('.alert').delay(6000).fadeOut();

    $(".calendario").datepicker({ format: "dd/mm/yyyy", language: "pt-BR", beforeShowDay: $.datepicker.noWeekends });

    $('.menu-main').on('click', function (e) {
        e.preventDefault();
    });

    $(".area-imagem").on('click', function () {
        $(this).closest('div.form-group').find(".file-foto").css("display", 'block');
    });

    $('a.menu-link').on('click', function (e) {

        e.preventDefault();
        var id = $(this).data('id');
        var location = $(this).attr('href');

        $.ajax({
            url: caminho + '/Home/MenuClicado',
            type: "POST",
            data: {
                id: id
            },
            success: function (data) {
                window.location = location;
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $('.skumask').keyup(function () {
        this.value = this.value.replace(/[^0-9\/-]/g, '');
    });

    $('.numbers').keyup(function () {
        this.value = this.value.replace(/[^0-9\/]/g, '');
    });

    $(".valor").maskMoney({ prefix: 'R$ ', allowNegative: true, thousands: '.', decimal: ',', affixesStay: false });

    $(document).on('click', '.deletar', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');
        var action = $(this).attr('href');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '.finalizar', function (e) {
        e.preventDefault();

        var id = $(this).attr('data-id');
        var action = $(this).attr('href');

        bootbox.confirm({
            message: "<h3>Deseja realmente finalizar a turma?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "GET",
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    var behavior = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00000';
    };

    var options = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior.apply({}, arguments), options);
        }
    };

    $('.maskFone').mask(behavior, options);

    var behavior2 = function (val) {
        return val.replace(/\D/g, '').length === 9 ? '00000-000' : '00000-000';
    };

    var options2 = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior2.apply({}, arguments), options);
        }
    };

    $('.maskCep').mask(behavior2, options2);

    var behavior3 = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '000.000.000-00' : val.replace(/\D/g, '').length === 14 ? '00.000.000/0000-00' : '000.000.000-00';
    };

    var options3 = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behavior3.apply({}, arguments), options);
        }
    };

    $('.maskDOC').mask(behavior3, options3);

    var infInicial = "O campo "
    var varPreenc = " deve ser preenchido!<br>";
    var msg = '';

    $('.form_submit').click(function (e) {
        var form = $(this).closest('form').attr("id");

        msg = "";

        $("form#" + form + " input, form#" + form + " select, form#" + form + " textarea").each(function () {
            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                var input = $(this).attr('name');
                var verificar = $(this).data('verif');
                var nomecampo = $(this).data('name');

                if (verificar != false) {
                    if ($(this).val() === "" && input != 'foto') {
                        msg += infInicial + nomecampo + varPreenc;
                    }
                }
            }
        });

        if (msg != "") {
            $dispararAlerta(msg, 'warning');
            return false;
        } else {
            $('form[name=' + form + ']').submit();
        }

    });

    $dispararAlerta = function (msg, tipo, op = null) {

        if (op == 1) {
            $.UIkit.notify({
                message: msg,
                status: tipo,
                timeout: 10000,
                pos: 'top-right',
                onClose: function () {
                    location.reload();
                }
            });
        } else {
            $.UIkit.notify({
                message: msg,
                status: tipo,
                timeout: 10000,
                pos: 'top-right'
            });
        }

        return false;
    };

    $(".table-pages").dataTable({
        "order": [
            [1, "desc"]
        ],
        "language": {
            url: caminho + '/public_html/static/JavaScript/dataTable/lang/PT_BR.json'
        },
    });

    $(".ecep").on("blur", function () {
        var cep = $(this).val();

        if (cep != "") {
            var obj = $(this).closest('.area_endereco');

            obj.find(".elogradouro").val("");
            obj.find(".ebairro").val("");
            obj.find(".eestado").val("");
            obj.find(".ecidade").val("");
            obj.find(".epais").val("");

            var url = 'https://viacep.com.br/ws/' + cep + '/json/';

            $.get(url, function (data) {
                if (!("erro" in data)) {
                    if (Object.prototype.toString.call(data) === '[object Array]') {
                        var data = data[0];
                    }

                    obj.find(".elogradouro").val(data.logradouro.toUpperCase());
                    obj.find(".ebairro").val(data.bairro.toUpperCase());
                    obj.find(".eestado").val(data.uf.toUpperCase()).change();
                    obj.find(".ecidade").val(data.localidade.toUpperCase());
                    obj.find(".eibge").val(data.ibge);
                    obj.find(".epais").val('BRASIL');
                }
            });
        }
    });

    $(".dataTables_wrapper select").select2({
        minimunResultsForSearch: -1
    }); 
});
