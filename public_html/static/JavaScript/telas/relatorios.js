jQuery(window).load(function () {

    var buttonCommon = {
        exportOptions: {
            format: {
                body: function (data, row, column, node) {
                    return data;
                }
            }
        }
    };
    var $ = jQuery;

    $("#table-relatorios").DataTable({
        order: [[0, "DESC"]],
        dom: '<"row"<"col-sm-6 nopad_r"l><"col-sm-6 nopad_l"B>><"row"<"col-sm-12"tr>><"row"<"col-sm-6 nopad_r"i><"col-sm-6 nopad_l"p>>',
        "language": {
            url: caminho + '/public_html/static/JavaScript/dataTable/lang/PT_BR.json'
        },
        buttons: [
            $.extend(true, {}, buttonCommon, {
                extend: 'copyHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'excelHtml5'
            }),
            $.extend(true, {}, buttonCommon, {
                extend: 'pdfHtml5'
            })
        ]
    });
});
