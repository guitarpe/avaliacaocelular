jQuery(document).ready(function ($) {

    $(document).on('click', '.deletar_usuario', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });

    $(document).on('click', '.change_status', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');

        $.ajax({
            url: action,
            type: "POST",
            success: function (ret) {

                var data = JSON.parse(ret);

                if (data.O_COD_RETORNO == 0) {
                    $dispararAlerta(data.O_DESC_CURTO, 'success');
                    window.location.reload();
                } else {
                    $dispararAlerta('Erro: ' + data.O_COD_RETORNO + '-' + data.O_DESC_CURTO, 'danger');
                }
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });

    $(document).on('blur', '.camposenha', function () {
        if ($('input[name="senha"]').val() != '' && $('input[name="confsenha"]').val() != '') {
            if ($('input[name="senha"]').val() != $('input[name="confsenha"]').val()) {
                $dispararAlerta('Erro: A senha deve ser confirmada', 'danger');
                $('input[name="senha"]').val('');
                $('input[name="confsenha"]').val('');
            }
        }
    });


    $(document).on('blur', '#nome', function (e) {
        e.preventDefault();

        var value = $(this).val();

        $.ajax({
            url: caminho + '/Usuarios/RetornoLoginUsuario',
            type: "POST",
            data: {
                'nome': value
            },
            success: function (ret) {
                $('#login').val(ret);
            },
            error: function (jqXHR, textStatus, errorMessage) {
                console.log(errorMessage);
            }
        });
    });
});
