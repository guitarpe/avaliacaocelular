jQuery(document).ready(function ($) {

    $('.link_null').on('click', function (e) {
        e.preventDefault();

    });

    /*$('.pagina-main').waitMe({
        effect: 'strech',
        text: 'Carregando',
        bg: '#33333354',
        color: '#e77817',
        maxSize: '',
        waitTime: -1,
        textPos: 'vertical',
        fontSize: '',
        source: '',
        onClose: function () { }
    });*/

    $('.numbers').keyup(function () {
        this.value = this.value.replace(/[^0-9\/]/g, '');
    });

    $('.email').on('blur', function () {

        var sEmail = $(this).val();
        var emailFilter = /^.+@.+\..{2,}$/;
        var illegalChars = /[\(\)\<\>\,\;\:\\\/\"\[\]]/

        if (!(emailFilter.test(sEmail)) || sEmail.match(illegalChars)) {
            $(this).val('');
        }
    });

    var behaviordoc = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '000.000.000-00' : val.replace(/\D/g, '').length === 14 ? '00.000.000/0000-00' : '000.000.000-00';
    };

    var optionsdoc = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behaviordoc.apply({}, arguments), optionsdoc);
        }
    };

    $('.maskDoc').mask(behaviordoc, optionsdoc);

    var behaviortel = function (val) {
        return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00000';
    };

    var optionstel = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behaviortel.apply({}, arguments), optionstel);
        }
    };

    $('.maskFone').mask(behaviortel, optionstel);

    var behaviorcep = function (val) {
        return val.replace(/\D/g, '').length === 8 ? '00000-000' : '00000-000';
    };

    var optionscep = {
        onKeyPress: function (val, e, field, options) {
            field.mask(behaviorcep.apply({}, arguments), optionscep);
        }
    };

    $('.maskCep').mask(behaviorcep, optionscep);

    $('.validaCPF').on('blur', function () {

        var cpf = $(this).val().replace(/[^0-9]/g, '').toString();

        if (cpf.length > 0) {
            if (cpf.length == 11) {
                var v = [];

                v[0] = 1 * cpf[0] + 2 * cpf[1] + 3 * cpf[2];
                v[0] += 4 * cpf[3] + 5 * cpf[4] + 6 * cpf[5];
                v[0] += 7 * cpf[6] + 8 * cpf[7] + 9 * cpf[8];
                v[0] = v[0] % 11;
                v[0] = v[0] % 10;

                v[1] = 1 * cpf[1] + 2 * cpf[2] + 3 * cpf[3];
                v[1] += 4 * cpf[4] + 5 * cpf[5] + 6 * cpf[6];
                v[1] += 7 * cpf[7] + 8 * cpf[8] + 9 * v[0];
                v[1] = v[1] % 11;
                v[1] = v[1] % 10;

                if ((v[0] != cpf[9]) || (v[1] != cpf[10])) {
                    var msg = 'CPF inválido: ' + cpf;
                    $dispararAlerta(msg, 'warning');

                    $(this).val('');
                    $(this).focus();
                }
            } else {
                var msg = 'CPF inválido: ' + cpf;
                $dispararAlerta(msg, 'warning');

                $(this).val('');
                $(this).focus();
            }
        }
    });

    $('.form_submit').on('click', function (e) {

        var form = $(this).closest('form').attr("id");

        var infInicial = "O campo "
        var varPreenc = " deve ser preenchido!<br>";
        var msg = '';

        $("form#" + form + " input, form#" + form + " select, form#" + form + " textarea").each(function () {
            if ((!$(this).is('[readonly]')) && (!$(this).is('[disabled]'))) {

                var input = $(this).attr('name');
                var verificar = $(this).data('verif');
                var nomecampo = $(this).data('name');

                if (verificar != false) {
                    if ($(this).val() === "" && input != 'foto') {
                        msg += infInicial + nomecampo + varPreenc;
                    }
                }
            }
        });

        if (msg != "") {
            $dispararAlerta(msg, 'warning');
            return false;
        } else {
            //$('form[name=' + form + ']').submit();
        }
    });

    $dispararAlerta = function (msg, tipo, op = null) {

        if (op == 1) {
            $.UIkit.notify({
                message: msg,
                status: tipo,
                timeout: 10000,
                pos: 'top-right',
                onClose: function () {
                    location.reload();
                }
            });
        } else {
            $.UIkit.notify({
                message: msg,
                status: tipo,
                timeout: 10000,
                pos: 'top-right'
            });
        }

        return false;
    };

    $('.opavaria').prop('checked', false);

    $('.opavaria').on('click', function (e) {
        var campo = $(this).closest('.opcoes').find('input[name="avarias[]"');

        if ($(this).is(':checked') && $(this).val() != "") {
            campo.val($(this).val());
        } else {
            campo.val("");
        }
    });

    $('.brz-icon-svg').on('click', function (e) {
        e.preventDefault();
    });


    $('.selects-tela').select2({
        placeholder: "Selecione",
        allowClear: true,
        formatNoMatches: function () {
            return "Nenhum resultado encontrado";
        }
    });

    $(document).on('change', '#select-tipos', function () {
        window.location.href = $(this).val();
    });

    $(document).on('change', '#select-modelos', function () {
        window.location.href = $(this).val();
    });

    $('a.uk-close').on('click', function () {
        $(document).find('.uk-notify').remove();
    });

    if ($(document).width() > 1024) {
        $(window).scroll(function () {
            if ($(this).scrollTop() > 50) {
                $('#navbar_top').addClass("navbar-fixed-top");

                $('#navbar_top').animate({
                    top: '0px'
                });
            } else {
                $('#navbar_top').removeClass("navbar-fixed-top");
            }
        });
    }
});
