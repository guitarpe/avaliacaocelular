jQuery(document).ready(function ($) {

    $("#tipovideo").on("change", function () {
        var field = $(this).val();

        $("div.opcoes").addClass("hide");
        $("#tipo-" + field).removeClass("hide");
    });

});
