jQuery(document).ready(function ($) {

    $(document).on('click', '.deletar_menu', function (e) {
        e.preventDefault();

        var action = $(this).attr('href');
        var id = $(this).attr('data-id');

        bootbox.confirm({
            message: "<h3>Deseja realmente remover o registro?</h3>",
            buttons: {
                confirm: {
                    label: 'Sim',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'Não',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result == true) {
                    $.ajax({
                        url: action,
                        type: "POST",
                        data: {
                            id: id
                        },
                        success: function (data) {
                            window.location.href = data;
                        },
                        error: function (jqXHR, textStatus, errorMessage) {
                            console.log(errorMessage);
                        }
                    });
                }
            }
        });
    });
});
