<?php

define('APPLICATION_PATH', realpath(dirname(__FILE__)) . '/../src');
define('SITE_PATH', realpath(dirname(__FILE__)) . '/');
define('VIEW_PATH', APPLICATION_PATH . '/Application/View');

set_include_path(
    SITE_PATH . '../vendor' . PATH_SEPARATOR .
    APPLICATION_PATH . PATH_SEPARATOR .
    get_include_path()
);

require_once 'avalcelular/AutoLoader.php';
require_once '../config/db.php';
require_once '../config/config.php';

require_once (SYSTEM_PATH . 'vendor/avalcelular/Constantes.php');
require_once (SYSTEM_PATH . 'vendor/avalcelular/class.phpmailer.php');
require_once (SYSTEM_PATH . 'vendor/avalcelular/class.smtp.php');

use avalcelular\AutoLoader;
use avalcelular\Router;
use avalcelular\Request;

try {
    $loader = new AutoLoader();
    $loader->register();

    Router::run(new Request());
} catch (\Exception $e) {
    new Application\Controller\Error($e->getMessage(), $e->getLine(), $e->getFile());
}
